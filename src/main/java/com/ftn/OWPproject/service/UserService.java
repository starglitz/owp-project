package com.ftn.OWPproject.service;

import java.util.List;

import com.ftn.OWPproject.model.User;


public interface UserService {
	
	User findOne(String username);
	User findOne(String username, String password);
	List<User> findAll();
	User save(User user);


	User update(User user);


	User delete(String username);
	
	
}
