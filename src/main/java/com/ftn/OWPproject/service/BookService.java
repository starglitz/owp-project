package com.ftn.OWPproject.service;

import java.util.List;

import com.ftn.OWPproject.model.Book;
import com.ftn.OWPproject.model.Genre;


public interface BookService {
	
	Book findOne(Long id);
	List<Book> findAll();
	Book save(Book book);
	List<Book> save(List<Book> books);
	Book update(Book book);
	List<Book> update(List<Book> books);
	Book delete(Long id);
	List<Book> deleteAll(Genre genre);
	void delete(List<Long> ids);
	public List<Book> find(String name, Integer minPrice, Integer maxPrice,String author,String language, Long genreId, Long ISBN);
	List<Book> findByGenreId(Long genreId);
	public List<Book> sort(String sortCriteria, String ascDesc, List<Book> unsorted);
}
