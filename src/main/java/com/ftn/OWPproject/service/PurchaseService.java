package com.ftn.OWPproject.service;

import java.util.List;


import com.ftn.OWPproject.model.Purchase;

public interface PurchaseService {
	
	public Purchase findOne(Long id);

	public List<Purchase> findAll();

	public Purchase save(Purchase purchase);

	public Purchase update(Purchase purchase);
	
	public List<Purchase> findPurchasesByUser(Long userID);
}
