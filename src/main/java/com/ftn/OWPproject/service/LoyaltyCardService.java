package com.ftn.OWPproject.service;

import java.util.List;

import com.ftn.OWPproject.model.LoyaltyCard;

public interface LoyaltyCardService {

	public LoyaltyCard findOne(Long id);

	public List<LoyaltyCard> findAll();

	public void save(LoyaltyCard card);

	public void update(LoyaltyCard card);
	
	public LoyaltyCard findByUserID(Long userID);
	
	public void delete(Long id);
	
	public List<LoyaltyCard> findPending();
	
}
