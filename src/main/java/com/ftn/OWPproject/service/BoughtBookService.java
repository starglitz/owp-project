package com.ftn.OWPproject.service;

import java.util.List;

import com.ftn.OWPproject.model.BoughtBook;

public interface BoughtBookService {
	public BoughtBook findOne(Long id);

	public List<BoughtBook> findAll();

	public BoughtBook save(BoughtBook book);

	public BoughtBook update(BoughtBook book);
}
