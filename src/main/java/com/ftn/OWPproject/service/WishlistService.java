package com.ftn.OWPproject.service;

import java.util.List;

import com.ftn.OWPproject.model.Wishlist;
import com.ftn.OWPproject.model.WishlistDB;

public interface WishlistService {

	public Wishlist findByUserId(Long userID);

	public void save(Long userID, Long bookID);

	public void delete(Long userID, Long bookID);
	
	
}
