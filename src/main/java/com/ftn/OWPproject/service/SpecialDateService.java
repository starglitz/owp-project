package com.ftn.OWPproject.service;

import java.time.LocalDate;
import java.util.List;

import com.ftn.OWPproject.model.SpecialDate;

public interface SpecialDateService {

	public SpecialDate findOne(LocalDate date);

	public List<SpecialDate> findAll();

	public void save(SpecialDate date);
	
	public boolean isTodaySpecialDate();
	
	public boolean doesDateAlreadyExist(LocalDate date);
	
}
