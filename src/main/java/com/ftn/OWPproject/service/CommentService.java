package com.ftn.OWPproject.service;

import java.util.List;

import com.ftn.OWPproject.model.Comment;

public interface CommentService {

	public Comment findOne(Long id);

	public List<Comment> findAll();

	public void save(Comment comment);

	public void update(Comment comment);

	public void delete(Long id);
	
	public List<Comment> findByUser(Long userID);
	
	public List<Comment> findAllApproved();
	
	public List<Comment> findAllPending();
	
	public List<Comment> findByUserAndBook(Long userID, Long BookID);
	
	public List<Comment> findByBook(Long bookID);
	
}
