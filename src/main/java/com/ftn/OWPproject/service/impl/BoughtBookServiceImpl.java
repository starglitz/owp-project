package com.ftn.OWPproject.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftn.OWPproject.dao.BoughtBookDAO;
import com.ftn.OWPproject.model.BoughtBook;
import com.ftn.OWPproject.service.BoughtBookService;

@Service
public class BoughtBookServiceImpl implements BoughtBookService{

	@Autowired
	private BoughtBookDAO boughtBookDAO;
	
	
	@Override
	public BoughtBook findOne(Long id) {
		return boughtBookDAO.findOne(id);
	}

	@Override
	public List<BoughtBook> findAll() {
		return boughtBookDAO.findAll();
	}

	@Override
	public BoughtBook save(BoughtBook book) {
		boughtBookDAO.save(book);
		return book;
	}

	@Override
	public BoughtBook update(BoughtBook book) {
		boughtBookDAO.update(book);
		return book;
	}

	
	
}
