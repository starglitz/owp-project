package com.ftn.OWPproject.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftn.OWPproject.dao.LoyaltyCardDAO;
import com.ftn.OWPproject.model.LoyaltyCard;
import com.ftn.OWPproject.model.LoyaltyCardStatus;
import com.ftn.OWPproject.service.LoyaltyCardService;

@Service
public class LoyaltyCardServiceImpl implements LoyaltyCardService{

	
	@Autowired
	private LoyaltyCardDAO loyaltyCardDAO;
	
	public LoyaltyCard findOne(Long id) {
		return loyaltyCardDAO.findOne(id);
	}

	public List<LoyaltyCard> findAll() {
		return loyaltyCardDAO.findAll();
	}

	public void save(LoyaltyCard card) {
		loyaltyCardDAO.save(card);
	}

	public void update(LoyaltyCard card) {
		loyaltyCardDAO.update(card);
	}
	
	public LoyaltyCard findByUserID(Long userID) {
		return loyaltyCardDAO.findByUserID(userID);
	}
	
	public void delete(Long id) {
		loyaltyCardDAO.delete(id);
	}
	
	public List<LoyaltyCard> findPending() {
		List<LoyaltyCard> allCards = loyaltyCardDAO.findAll();
		List<LoyaltyCard> pending = new ArrayList<LoyaltyCard>();
		for(LoyaltyCard i : allCards) {
			if(i.getStatus().equals(LoyaltyCardStatus.PENDING)) {
				pending.add(i);
			}
		}
		return pending;
	}
}
