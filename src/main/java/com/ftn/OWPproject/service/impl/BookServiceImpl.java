package com.ftn.OWPproject.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftn.OWPproject.dao.BookDAO;
import com.ftn.OWPproject.model.Book;
import com.ftn.OWPproject.model.Genre;
import com.ftn.OWPproject.service.BookService;


@Service
public class BookServiceImpl implements BookService{
	
	@Autowired
	private BookDAO bookDAO;

	@Override
	public Book findOne(Long id) {
		return bookDAO.findOne(id);
	}

	@Override
	public List<Book> findAll() {
		return bookDAO.findAll();
	}

	@Override
	public Book save(Book book) {
		bookDAO.save(book);
		return book;
	}

	@Override
	public List<Book> save(List<Book> books) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Book update(Book book) {
		bookDAO.update(book);
		return book;
	}

	@Override
	public List<Book> update(List<Book> books) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Book delete(Long id) {
		Book book = findOne(id);
		if (book != null)
			bookDAO.delete(id);
		
		return book;
	}

	@Override
	public List<Book> deleteAll(Genre genre) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(List<Long> ids) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Book> find(String name, Integer minPrice, Integer maxPrice, String author, String language,
			Long genreId, Long ISBN) {
		List<Book> books = bookDAO.findAll();
		

		if (name == null) {
			name = "";
		}
		
		if (author == null) {
			author = "";
		}
		
		if (language == null) {
			language = "";
		}
		
		if (genreId == null) {
			genreId = 0L;
		}
		if (minPrice == null) {
			minPrice = 0;
		}
		if (maxPrice == null) {
			maxPrice = Integer.MAX_VALUE;
		}
		
		List<Book> rezultat = new ArrayList<>();
		for (Book itBook: books) {
			// kriterijum pretrage
			if (!itBook.getTitle().toLowerCase().contains(name.toLowerCase())) {
				continue;
			}
			if (!itBook.getAuthor().toLowerCase().contains(author.toLowerCase())) {
				continue;
			}
			if (!itBook.getLanguage().toLowerCase().contains(language.toLowerCase())) {
				continue;
			}
			if (genreId > 0) { // ako je žanr odabran
				boolean pronadjen = false;
				for (Genre itGenre: itBook.getGenres()) {
					if (itGenre.getID() == genreId) {
						pronadjen = true;
						break;
					}
				}
				if (!pronadjen) {
					continue;
				}
			}
			if (!(itBook.getPrice() >= minPrice && itBook.getPrice() <= maxPrice)) {
				continue;
			}
			if(ISBN != null) {
			if (!(itBook.getISBN().equals(ISBN))) {
				continue;
			} }
			

			rezultat.add(itBook);
		}

		return rezultat;
	}
	
	public List<Book> sort(String sortCriteria, String ascDesc, List<Book> unsorted) {
		if(sortCriteria.equals("rating")) {
			if(ascDesc.equals("asc")) {
				Collections.sort(unsorted, Comparator.comparingDouble(Book ::getRating));
			}
			else {
				Collections.sort(unsorted, Comparator.comparingDouble(Book ::getRating).reversed());
			}
		}
		
		if(sortCriteria.equals("price")) {
			if(ascDesc.equals("asc")) {
				Collections.sort(unsorted, Comparator.comparingInt(Book ::getPrice));
			}
			else {
				Collections.sort(unsorted, Comparator.comparingInt(Book ::getPrice).reversed());
			}
		}
		
		
		else if(sortCriteria.equals("language")) {
			if(ascDesc.equals("asc")) {
				Collections.sort(unsorted, Comparator.comparing(Book::getLanguage));
			}
			else {
				Collections.sort(unsorted, Comparator.comparing(Book::getLanguage).reversed());
			}
		}
		
		else if(sortCriteria.equals("author")) {
			if(ascDesc.equals("asc")) {
				Collections.sort(unsorted, Comparator.comparing(Book::getAuthor));
			}
			else {
				Collections.sort(unsorted, Comparator.comparing(Book::getAuthor).reversed());
			}
		}
		
		
		
		
		return unsorted;
	}

	@Override
	public List<Book> findByGenreId(Long genreId) {
		// TODO Auto-generated method stub
		return null;
	}

}
