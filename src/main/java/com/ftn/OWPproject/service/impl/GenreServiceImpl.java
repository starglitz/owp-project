package com.ftn.OWPproject.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftn.OWPproject.dao.GenreDAO;
import com.ftn.OWPproject.model.Genre;
import com.ftn.OWPproject.service.GenreService;


@Service
public class GenreServiceImpl implements GenreService {

	@Autowired
	private GenreDAO genreDAO;
	
	@Override
	public Genre findOne(Long id) {
		return genreDAO.findOne(id);
	}

	@Override
	public List<Genre> findAll() {
		return genreDAO.findAll();
	}

	@Override
	public List<Genre> find(Long[] ids) {
		List<Genre> rezultat = new ArrayList<>();
		for (Long id: ids) {
			Genre genre = genreDAO.findOne(id);
			rezultat.add(genre);
		}

		return rezultat;
	}

	

		
}
