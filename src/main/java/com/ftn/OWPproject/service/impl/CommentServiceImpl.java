package com.ftn.OWPproject.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftn.OWPproject.dao.CommentDAO;
import com.ftn.OWPproject.model.Comment;
import com.ftn.OWPproject.service.CommentService;

@Service
public class CommentServiceImpl implements CommentService{

	@Autowired
	private CommentDAO commentDAO;
	
	
	@Override
	public Comment findOne(Long id) {
		return commentDAO.findOne(id);
	}

	@Override
	public List<Comment> findAll() {
		return commentDAO.findAll();
	}

	@Override
	public void save(Comment comment) {
		commentDAO.save(comment);
		
	}

	@Override
	public void update(Comment comment) {
		commentDAO.update(comment);
		
	}

	@Override
	public void delete(Long id) {
	   commentDAO.delete(id);
		
	}

	@Override
	public List<Comment> findByUser(Long userID) {
		return commentDAO.findByUser(userID);
	}

	@Override
	public List<Comment> findAllApproved() {
		return commentDAO.findAllApproved();
	}

	@Override
	public List<Comment> findAllPending() {
		 return commentDAO.findAllPending();
	}
	
	@Override
	public List<Comment> findByUserAndBook(Long userID, Long BookID) {
		return commentDAO.findByUserAndBook(userID, BookID);
	}
	
	@Override
	public List<Comment> findByBook(Long bookID) {
		return commentDAO.findByBook(bookID);
	}

	
	
}
