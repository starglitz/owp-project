package com.ftn.OWPproject.service.impl;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftn.OWPproject.dao.SpecialDateDAO;
import com.ftn.OWPproject.model.SpecialDate;
import com.ftn.OWPproject.service.SpecialDateService;

@Service
public class SpecialDateServiceImpl implements SpecialDateService{

	@Autowired
	private SpecialDateDAO specialDateDAO;

	@Override
	public SpecialDate findOne(LocalDate date) {
		return specialDateDAO.findOne(date);
	}

	@Override
	public List<SpecialDate> findAll() {
		return specialDateDAO.findAll();
	}

	@Override
	public void save(SpecialDate date) {
		specialDateDAO.save(date);
		
	}

	@Override
	public boolean isTodaySpecialDate() {
		return specialDateDAO.isTodaySpecialDate();
	}

	@Override
	public boolean doesDateAlreadyExist(LocalDate date) {
		return specialDateDAO.doesDateAlreadyExist(date);
	}
	
	
	
}
