package com.ftn.OWPproject.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftn.OWPproject.dao.WishlistDAO;
import com.ftn.OWPproject.model.Wishlist;
import com.ftn.OWPproject.model.WishlistDB;
import com.ftn.OWPproject.service.WishlistService;


@Service
public class WishlistServiceImpl implements WishlistService{

	@Autowired
	private WishlistDAO wishlistDAO;
	
	@Override
	public Wishlist findByUserId(Long userID) {
		return wishlistDAO.findByUserId(userID);
	}

	@Override
	public void save(Long userID, Long bookID) {
		wishlistDAO.save(userID, bookID);
		
	}

	@Override
	public void delete(Long userID, Long bookID) {
		wishlistDAO.delete(userID, bookID);
		
	}

	

}
