package com.ftn.OWPproject.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftn.OWPproject.dao.UserDAO;
import com.ftn.OWPproject.model.User;
import com.ftn.OWPproject.service.UserService;

@Service
public class UserServiceImpl implements UserService{

	@Autowired
	private UserDAO userDAO;
	
	
	@Override
	public User findOne(String username) {
		return userDAO.findOne(username);
	}

	@Override
	public User findOne(String username, String password) {
		return userDAO.findOne(username, password);
	}

	@Override
	public List<User> findAll() {
		return userDAO.findAll();
	}

	@Override
	public User save(User user) {
		userDAO.save(user);
		return user;
	}

	

	@Override
	public User update(User user) {
		userDAO.update(user);
		return user;
	}

	

	@Override
	public User delete(String username) {
		User user = findOne(username);
		if (user != null) {
			userDAO.delete(username);
		}
		return user;
	}
	
	
}
