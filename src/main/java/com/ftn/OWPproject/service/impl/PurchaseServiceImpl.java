package com.ftn.OWPproject.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftn.OWPproject.dao.PurchaseDAO;
import com.ftn.OWPproject.model.Purchase;
import com.ftn.OWPproject.service.PurchaseService;

@Service
public class PurchaseServiceImpl implements PurchaseService{

	@Autowired
	private PurchaseDAO purchaseDAO;
	
	@Override
	public Purchase findOne(Long id) {
		return purchaseDAO.findOne(id);
	}

	@Override
	public List<Purchase> findAll() {
		return purchaseDAO.findAll();
	}

	@Override
	public Purchase save(Purchase purchase) {
		purchaseDAO.save(purchase);
		return purchase;
	}

	@Override
	public Purchase update(Purchase purchase) {
		purchaseDAO.update(purchase);
		return purchase;
	}
	
	@Override
	public List<Purchase> findPurchasesByUser(Long userID) {
		return purchaseDAO.findPurchasesByUser(userID);
	}
	
	
}
