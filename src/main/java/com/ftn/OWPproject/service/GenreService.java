package com.ftn.OWPproject.service;

import java.util.List;

import com.ftn.OWPproject.model.Genre;


public interface GenreService {
	
	Genre findOne(Long id);
	List<Genre> findAll();
	List<Genre> find(Long[] ids);
	
}
