package com.ftn.OWPproject;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.ftn.OWPproject.dao.BoughtBookDAO;
import com.ftn.OWPproject.dao.impl.BoughtBookDAOimpl;
import com.ftn.OWPproject.model.BoughtBook;

@SpringBootApplication
public class OwPprojectApplication {

	
	
	public static void main(String[] args) {
		SpringApplication.run(OwPprojectApplication.class, args);
	
	
		
	}

}
