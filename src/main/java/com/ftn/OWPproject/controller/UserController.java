package com.ftn.OWPproject.controller;

import com.ftn.OWPproject.model.*;
/*import com.ftn.OWPproject.model.Book;
import com.ftn.OWPproject.model.User;*/
import com.ftn.OWPproject.service.*;
/*import com.ftn.OWPproject.service.BookService;
import com.ftn.OWPproject.service.UserService;*/
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping(value = "/Users")
public class UserController {

	public static final String USER_KEY = "currentUser";

	@Autowired
	private UserService userService;

	@Autowired
	private BookService bookService;
	
	@Autowired
	private PurchaseService purchaseService;
	
	@Autowired
	private WishlistService wishlistService;

	@PostMapping(value = "/Login")
	public ModelAndView login(@RequestParam String username, @RequestParam String password, HttpSession session,
			HttpServletResponse response) throws IOException {
		List<Book> books = bookService.findAll();

		// validacija
		User user = userService.findOne(username, password);
		if (user == null) {

			ModelAndView rezultat = new ModelAndView("books");
			rezultat.addObject("books", books);
			rezultat.addObject("loginError", "The credentials you entered are invalid. Try again");
			return rezultat;

		} else {
			if(user.isBlocked() == true) {
				ModelAndView rezultat = new ModelAndView("books");
				rezultat.addObject("books", books);
				rezultat.addObject("loginError", "You are blocked from using this site. Shoo!");
				return rezultat;
			}
			else {
			session.setAttribute(UserController.USER_KEY, user);
			ModelMap rezultat2 = new ModelMap();
			rezultat2.addAttribute("books", books);
			// return "redirect:/";
			return new ModelAndView("redirect:/", rezultat2);
			}
		}
		

	}

	@GetMapping(value = "/Register")
	public ModelAndView register(HttpSession session, HttpServletResponse response) throws IOException {
		
		User userSession =(User)session.getAttribute("currentUser");
		if(userSession != null) {
			response.sendRedirect("/LaLivret/");
		}
		
		return new ModelAndView("register");
	}

	@PostMapping(value = "/Register")
	public ModelAndView register(@Valid User user, BindingResult bindingResult, @RequestParam String password,
			@RequestParam String passwordConfirm ,HttpSession session, HttpServletResponse response)
			throws IOException {

		
	
		  if(!password.equals(passwordConfirm)) { 
			  ObjectError error = new
			  ObjectError("passwordConfirm", "Password mismatch.");
		       bindingResult.addError(error); 
		       }
		 

		if (bindingResult.hasErrors()) {
			ModelAndView rezultat = new ModelAndView("register");
			List<Book> books = bookService.findAll();
			rezultat.addObject("user", user);

			if (!password.equals(passwordConfirm)) {
				rezultat.addObject("passwordMismatch", "passMismatch");
			}

			return rezultat;
		}

		else {
			System.out.println(user);
			 User save = userService.save(user); 
			 
			List<Book> books = bookService.findAll();
			 ModelAndView rezultat = new ModelAndView("books");
				rezultat.addObject("books", books);
				rezultat.addObject("registerSuccess", "registerSuccess");
				return rezultat;
		}

	}
	
	@GetMapping(value="/Logout")
	public void logout(HttpSession session, HttpServletResponse response) throws IOException {
		// odjava	
		session.invalidate();
		
		response.sendRedirect("/LaLivret/");
	}
	
	@GetMapping(value="/CurrentUserDetails")
	public ModelAndView myDetails(HttpSession session, HttpServletResponse response) throws IOException {
		ModelAndView rezultat = new ModelAndView("currentUser");
		User user = (User) session.getAttribute("currentUser");
		List<Purchase> purchases = purchaseService.findPurchasesByUser(user.getID());
		rezultat.addObject("purchases", purchases);
		Wishlist usersWishlist = wishlistService.findByUserId(user.getID());
		List<Book> wishlistBooks = usersWishlist.getBooks();
		rezultat.addObject("wishlist", wishlistBooks);
		/* rezultat.addObject("user", (User) session.getAttribute(USER_KEY)); */
		return rezultat;
	}

	
	@GetMapping(value="/UserDetails")
	public ModelAndView details(@RequestParam String username) {
		User user = userService.findOne(username);
		ModelAndView rezultat = new ModelAndView("userDetails");
		
		List<Purchase> usersPurchases = purchaseService.findPurchasesByUser(user.getID());
		rezultat.addObject("user", user);
		rezultat.addObject("purchases", usersPurchases);
		
		return rezultat;
	}
	
	@GetMapping(value="/AllUsers")
	public ModelAndView allUsers() {
		List<User> users = userService.findAll();
		ModelAndView rezultat = new ModelAndView("allUsers");
		rezultat.addObject("users", users);
		return rezultat;
	}
	
	@PostMapping(value="/Block")
	public void block(@RequestParam String username, HttpServletResponse response) throws IOException {
		User user = userService.findOne(username);
		user.setBlocked(true);
		userService.update(user);
		response.sendRedirect("/LaLivret/");
	}
	
	@PostMapping(value="/ChangeRole")
	public void changeRole(@RequestParam String username, @RequestParam String role,HttpServletResponse response) throws IOException {
		User user = userService.findOne(username);
		user.setRole(role);
		userService.update(user);
		response.sendRedirect("/LaLivret/Users/UserDetails?username=" + user.getUsername());
	}
	
}
