package com.ftn.OWPproject.controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.ftn.OWPproject.model.Purchase;
import com.ftn.OWPproject.model.User;
import com.ftn.OWPproject.service.PurchaseService;

@Controller
@RequestMapping(value = "/Purchases")
public class PurchaseController {

	@Autowired
	private PurchaseService purchaseService;
	
	@GetMapping(value="/PurchaseDetails")
	public ModelAndView details(@RequestParam Long purchaseID) {
		Purchase purchase = purchaseService.findOne(purchaseID);
		ModelAndView rezultat = new ModelAndView("purchaseDetails");
		rezultat.addObject("purchase", purchase);
		return rezultat;
	}
	
	@GetMapping(value="/Reports")
	public ModelAndView reports(@RequestParam(required=false) String startDate, @RequestParam(required=false) String endDate) {
		
		  DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-d");

		  
		  List<Purchase> allPurchases = purchaseService.findAll();
		  ModelAndView rezultat = new ModelAndView("reports");
		  
		 
		  if(startDate != null && endDate != null && startDate != "" && endDate != "") {
		  LocalDate startDateLocal = LocalDate.parse(startDate, formatter);
		  LocalDate endDateLocal = LocalDate.parse(endDate, formatter);
		  
		  
		  if(startDateLocal.isAfter(endDateLocal)) {
				rezultat.addObject("error", "Start date must be before the end date");
			}
		  
		  allPurchases = getPurchaseInDateRange(allPurchases, startDateLocal, endDateLocal);
		  
		  }
		  
		int finalCount = 0;
		int finalPrice = 0;
		for(Purchase purchase : allPurchases) {
			finalCount += purchase.getNumberOfBooks();
			finalPrice += purchase.getPrice();
		}
		
		rezultat.addObject("finalCount", finalCount);
		rezultat.addObject("finalPrice", finalPrice);
		rezultat.addObject("purchases", allPurchases);
		return rezultat;
		
	}
	
	public List<Purchase> getPurchaseInDateRange(List<Purchase> purchases,LocalDate startDate,LocalDate endDate) {
		List<Purchase> purchasesInRange = new ArrayList<Purchase>();
		for(Purchase purchase : purchases) {
			if(purchase.getPurchaseDate().isAfter(startDate) && purchase.getPurchaseDate().isBefore(endDate)) {
				purchasesInRange.add(purchase);
			}
		}
		return purchasesInRange;
	}
	
}
