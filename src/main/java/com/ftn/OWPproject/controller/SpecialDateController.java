package com.ftn.OWPproject.controller;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.ftn.OWPproject.model.SpecialDate;
import com.ftn.OWPproject.service.SpecialDateService;

@Controller
@RequestMapping("/SpecialDate")
public class SpecialDateController {

	@Autowired
	private SpecialDateService specialDateService;
	
	@GetMapping()
	public ModelAndView specialDates() {
		
		List<SpecialDate> allDates = specialDateService.findAll();
		ModelAndView rezultat = new ModelAndView("specialDates");
		rezultat.addObject("specialDates", allDates);
		return rezultat;
		
	}
	
	@PostMapping("/AddSpecialDate")
	public String addSpecialDate(@RequestParam(required=false) String date, @RequestParam(required=false) Integer discount,
			RedirectAttributes redirectAttributes) {
		if(date == null || discount == null) {
			redirectAttributes.addFlashAttribute("errors", "You need to pick date and discount!");
		}
		
		else {
		LocalDate specialDate = LocalDate.parse(date);
		double discountDbl = discount.intValue() * 0.01;
		SpecialDate spec = new SpecialDate(specialDate, discountDbl);
		specialDateService.save(spec);
		
		}
		
		return "redirect:/SpecialDate";
	}
	
	
}
