package com.ftn.OWPproject.controller;

import java.util.List;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.ftn.OWPproject.model.Book;
import com.ftn.OWPproject.model.Comment;
import com.ftn.OWPproject.model.CommentStatus;
import com.ftn.OWPproject.model.User;
import com.ftn.OWPproject.service.BookService;
import com.ftn.OWPproject.service.CommentService;
import com.ftn.OWPproject.service.UserService;

@Controller
@RequestMapping(value = "/Comments")
public class CommentController {

	@Autowired
	private CommentService commentService;
	
	@Autowired
	private BookService bookService;
	
	@PostMapping(value="AddComment")
	public String addComment(@RequestParam(required=false) Integer rating, @RequestParam(required=false) String description,
			HttpSession session, HttpServletResponse response, @RequestParam Long bookID,  RedirectAttributes redirectAttributes) throws IOException {
		if(rating == null || description == null || description == "") {
			redirectAttributes.addFlashAttribute("errors", "You need to write something!");
	        return "redirect:/BookDetails?id=" + bookID;
		}
		else {
			
		User user = (User) session.getAttribute("currentUser");
		Book book = bookService.findOne(bookID);
		Comment comment = new Comment(description, rating, LocalDate.now(), user, book.getID(), CommentStatus.PENDING);
		commentService.save(comment);
		
		return "redirect:/";
		}
	}
	
	@GetMapping(value="PendingComments")
	public ModelAndView pendingComments() {
		ModelAndView rezultat = new ModelAndView("comments");
		List<Comment> pendingComments = commentService.findAllPending();
		rezultat.addObject("comments", pendingComments);
		return rezultat;
	}
	
	@PostMapping(value="ApproveComment")
	public void approveComment(@RequestParam Long commentID, HttpServletResponse response) throws IOException {
		Comment comment = commentService.findOne(commentID);
		comment.setStatus(CommentStatus.APPROVED);
		commentService.update(comment);
		response.sendRedirect("/LaLivret/Comments/PendingComments");
	}
	
	@PostMapping(value="DeleteComment")
	public void deleteComment(@RequestParam Long commentID, HttpServletResponse response) throws IOException {
		Comment comment = commentService.findOne(commentID);
		comment.setStatus(CommentStatus.REJECTED);
		commentService.update(comment);
		response.sendRedirect("/LaLivret/Comments/PendingComments");
	}
}
