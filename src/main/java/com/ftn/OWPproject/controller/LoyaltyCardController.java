package com.ftn.OWPproject.controller;

import com.ftn.OWPproject.model.*;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.ftn.OWPproject.service.BookService;
import com.ftn.OWPproject.service.LoyaltyCardService;
import com.ftn.OWPproject.service.UserService;

@Controller
@RequestMapping(value = "/LoyaltyCard")
public class LoyaltyCardController {

	@Autowired
	private LoyaltyCardService loyaltyCardService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private BookService bookService;
	
	@PostMapping(value="/Request")
	public ModelAndView request(@RequestParam String username) {
		
		User user = userService.findOne(username);
		
		LoyaltyCard card = loyaltyCardService.findByUserID(user.getID());
		
		List<Book> books = bookService.findAll();
		 ModelAndView rezultat = new ModelAndView("books");
		rezultat.addObject("books", books);
		if(card != null) {
			
			if(card.getStatus().equals(LoyaltyCardStatus.PENDING)) {
				rezultat.addObject("pending", "pending");
				return rezultat;
			}
			else {
				rezultat.addObject("hasCard", "hasCard");
				return rezultat;
			}
		}
		else {
			
			LoyaltyCard newCard = new LoyaltyCard(0.2, 4, user, LoyaltyCardStatus.PENDING);
			loyaltyCardService.save(newCard);
			
			rezultat.addObject("successCard", "successCard");
			return rezultat;
		}
		
	}
	
	@GetMapping(value="/AllRequests")
	public ModelAndView allRequests() {
		List<LoyaltyCard> pending = loyaltyCardService.findPending();
		 ModelAndView rezultat = new ModelAndView("allRequests");
			rezultat.addObject("requests", pending);
			return rezultat;
	}
	
	@PostMapping(value="/AcceptCard") 
	public void acceptCard(@RequestParam Long ID, HttpServletResponse response) throws IOException {
		LoyaltyCard card = loyaltyCardService.findOne(ID);
		card.setStatus(LoyaltyCardStatus.ACCEPTED);
		loyaltyCardService.update(card);
		response.sendRedirect("/LaLivret/");
	}
	
	
	@PostMapping(value="/DeclineCard") 
	public void declineCard(@RequestParam Long ID, HttpServletResponse response) throws IOException {
		loyaltyCardService.delete(ID);;
		response.sendRedirect("/LaLivret/");
	}
	
}
