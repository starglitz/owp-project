package com.ftn.OWPproject.controller;

import java.io.IOException;
import java.io.*;
import java.nio.file.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.tomcat.util.buf.StringUtils;
import org.apache.tomcat.util.http.fileupload.FileUpload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.ftn.OWPproject.model.Cover;
import com.ftn.OWPproject.model.Genre;
import com.ftn.OWPproject.model.LoyaltyCard;
import com.ftn.OWPproject.model.Purchase;
import com.ftn.OWPproject.model.Role;
import com.ftn.OWPproject.model.SpecialDate;
import com.ftn.OWPproject.model.User;
import com.ftn.OWPproject.model.Wishlist;
import com.ftn.OWPproject.dao.WishlistDAO;
import com.ftn.OWPproject.model.Book;
import com.ftn.OWPproject.model.BoughtBook;
import com.ftn.OWPproject.model.Comment;
import com.ftn.OWPproject.model.Writing;
import com.ftn.OWPproject.service.BookService;
import com.ftn.OWPproject.service.BoughtBookService;
import com.ftn.OWPproject.service.CommentService;
import com.ftn.OWPproject.service.GenreService;
import com.ftn.OWPproject.service.LoyaltyCardService;
import com.ftn.OWPproject.service.PurchaseService;
import com.ftn.OWPproject.service.SpecialDateService;
import com.ftn.OWPproject.service.UserService;
import com.ftn.OWPproject.service.WishlistService;
import com.ftn.OWPproject.service.impl.FileUploadUtil;

@Controller
@RequestMapping()
public class BookController {

	@Autowired
	private BookService bookService;

	@Autowired
	private GenreService genreService;

	@Autowired
	private BoughtBookService boughtBookService;
	
	@Autowired
	private PurchaseService purchaseService;
	
	@Autowired
	private LoyaltyCardService loyaltyCardService;
	
	@Autowired 
	private WishlistService wishlistService;
	
	@Autowired 
	private CommentService commentService;
	
	@Autowired
	private SpecialDateService specialDateService;
	
	@Autowired
	private ServletContext servletContext;
	private String baseURL;

	@PostConstruct
	public void init() {
		baseURL = servletContext.getContextPath() + "/";
	}

	@GetMapping
	public ModelAndView index(@RequestParam(required = false) Integer minPrice,
			@RequestParam(required = false) Integer maxPrice, @RequestParam(required = false) String title,
			@RequestParam(required = false) String author, @RequestParam(required = false) String language,
			@RequestParam(required = false) Long genreId, @RequestParam(required = false) Long ISBN,
			@RequestParam(required = false) String sortCriteria, @RequestParam(required = false) String ascDesc,

			HttpSession session) throws IOException {
		if (title != null && title.trim().equals(""))
			title = null;
		if (author != null && author.trim().equals(""))
			author = null;
		if (language != null && language.trim().equals(""))
			language = null;

		List<Book> books = bookService.find(title, minPrice, maxPrice, author, language, genreId, ISBN);
		if (sortCriteria != null && ascDesc != null) {
			books = bookService.sort(sortCriteria, ascDesc, books);
		}
		List<Genre> genres = genreService.findAll();

		
		
		
		
		// prosleđivanje
		ModelAndView rezultat = new ModelAndView("books");
		rezultat.addObject("books", books);
		rezultat.addObject("genres", genres);
		return rezultat;
	}

	@GetMapping("/BookDetails")
	public ModelAndView details(@RequestParam Long id, HttpSession session, @RequestParam(required=false) List<ObjectError> errors) {
		Book book = bookService.findOne(id);
		ModelAndView rezultat = new ModelAndView("bookDetails");
		rezultat.addObject("bookDetails", book);
		
		if(errors != null) {
			rezultat.addObject("errors", "You need to write a comment and add rating");
		}
		
		User user =(User)session.getAttribute("currentUser");
		
		if(user != null) {
		List<Comment> comment = commentService.findByUserAndBook(user.getID(), book.getID());
		if(comment != null && comment.size() == 0) {
			comment = null;
		}
		
		List<Purchase> purchases = purchaseService.findPurchasesByUser(user.getID());
		
		boolean canUserComment = checkIfUserCanComment(comment, purchases, user, book.getID());
		if(canUserComment) {
			rezultat.addObject("canComment", "canComment");
		}
		}
		
		List<Comment> booksComments = commentService.findByBook(book.getID());
		if(booksComments.size() != 0) {
			rezultat.addObject("booksComments", booksComments);
		}
		
		return rezultat;
	}
	
	private boolean checkIfUserCanComment(List<Comment> comments, List<Purchase> purchases, User user, Long BookID) {
		
		if(purchases != null && purchases.size() != 0) {
			for(Purchase purchase : purchases) {
				for(BoughtBook bbook : purchase.getBooks()) {
					if(bbook.getBookId().getID().equals(BookID) && comments == null && user.getRole().equals("CUSTOMER")){
						return true;
					}
				}
			}
		}
		
		return false;
		
	}
	
	
	

	@GetMapping("AddBook")
	public ModelAndView add(HttpSession session, HttpServletResponse response) throws IOException {
		
		User user =(User)session.getAttribute("currentUser");
		if(user == null || !user.getRole().equals("ADMINISTRATOR")) {
			response.sendRedirect("/LaLivret/");
		}
		
		ModelAndView rezultat = new ModelAndView("addBook");
		rezultat.addObject("addBook");
		List<Genre> genres = genreService.findAll();
		rezultat.addObject("genres", genres);
		return rezultat;
	}


	
	
	
	@PostMapping("AddBook")
	public ModelAndView add(@Valid Book book, BindingResult bindingResult,
			@RequestParam(name = "image", required = false) MultipartFile multipartFile,
			@RequestParam(name = "genreId", required = false) Long[] genreIds, HttpServletResponse response)
			throws IOException {
		System.out.println("i entered 1");

		if (multipartFile.isEmpty()) {
			ObjectError error = new ObjectError("picturePath", "You should import a picture.");
			bindingResult.addError(error);
		}

		if (bindingResult.hasErrors()) {
			System.out.println(bindingResult.getAllErrors());
			ModelAndView rezultat = new ModelAndView("addBook");
			rezultat.addObject("addBook", book);
			List<Genre> genres = genreService.findAll();
			rezultat.addObject("genres", genres);
			System.out.println(book);
			System.out.println("i entered to errors");
			// return "addBook";
			return rezultat;
		}

		else {

			System.out.println("entered");

			if (genreIds != null) {
				book.setGenres(genreService.find(genreIds));
			}

			// book.setGenres(genreService.find(genreIds));

			String fileName = multipartFile.getOriginalFilename();
			int startIndex = fileName.replaceAll("\\\\", "/").lastIndexOf("/");
			fileName = fileName.substring(startIndex + 1);
			book.setPicturePath(fileName);
			System.out.println(book);

			Book save = bookService.save(book);
			String uploadDir = "src/main/resources/static/images/";
			FileUploadUtil.saveFile(uploadDir, fileName, multipartFile);

			List<Book> books = bookService.findAll();
			ModelMap rezultat2 = new ModelMap();
			rezultat2.addAttribute("books", books);
			// return "redirect:/";
			return new ModelAndView("redirect:/", rezultat2);
		}

	}

	

	@GetMapping("EditBook")
	public ModelAndView edit(@RequestParam(name = "ID", required = false) Long ID, HttpSession session, HttpServletResponse response) throws IOException {
		
		User user =(User)session.getAttribute("currentUser");
		if(user == null || !user.getRole().equals("ADMINISTRATOR")) {
			response.sendRedirect("/LaLivret/");
		}
		
		Book book = bookService.findOne(ID);
		ModelAndView rezultat = new ModelAndView("changeBook");
		rezultat.addObject("bookChange", book);
		List<Genre> genres = genreService.findAll();
		rezultat.addObject("genres", genres);
		return rezultat;

	}

	/*
	 * @RequestParam Long ID , @RequestParam String title, @RequestParam Long ISBN,
	 * 
	 * @RequestParam String publisher, @RequestParam String author, @RequestParam
	 * String publishYear,
	 * 
	 * @RequestParam String description, @RequestParam int price, @RequestParam int
	 * numberOfPages,
	 * 
	 * @RequestParam String cover, @RequestParam String writing, @RequestParam
	 * String language,
	 * 
	 * @RequestParam int booksLeft, @RequestParam(name="genreId", required=false)
	 * Long[] genreIds, ) {
	 */

	@PostMapping("EditBook")
	public ModelAndView edit(@Valid Book book, BindingResult bindingResult,
			@RequestParam(name = "image", required = false) MultipartFile multipartFile,
			@RequestParam(name = "genreId", required = false) Long[] genreIds, HttpServletResponse response)
			throws IOException {

		
		  
		 

		System.out.println("i entered to edit book controller");

		if (bindingResult.hasErrors()) {
			System.out.println("i entered to edit book controller errors");
			System.out.println(bindingResult.getAllErrors());
			ModelAndView rezultat = new ModelAndView("changeBook");
			if(multipartFile.isEmpty()) { 
				Book bookFromDb = bookService.findOne(book.getID());
				book.setPicturePath(bookFromDb.getPicturePath());
			}
			rezultat.addObject("bookChange", book);
			List<Genre> genres = genreService.findAll();
			rezultat.addObject("genres", genres);
			System.out.println(book);
			System.out.println("i entered to errors");
			// return "addBook";
			return rezultat;
		}

		else {
			System.out.println("entered success");
			if (genreIds != null) {
				book.setGenres(genreService.find(genreIds));
			}

			
			if(!multipartFile.isEmpty()) { 
				String fileName = multipartFile.getOriginalFilename();
				int startIndex = fileName.replaceAll("\\\\", "/").lastIndexOf("/");
				fileName = fileName.substring(startIndex + 1);
				book.setPicturePath(fileName);
				System.out.println(book);

				
				String uploadDir = "src/main/resources/static/images/";
				FileUploadUtil.saveFile(uploadDir, fileName, multipartFile);
			  }
			
			else {
				Book bookFromDb = bookService.findOne(book.getID());
				
				String bookDbPath = bookFromDb.getPicturePath();
				
				String imgPath = bookDbPath.replaceAll("images/", "");
				
				book.setPicturePath(imgPath);
				System.out.println(bookFromDb);
				System.out.println(book);
			}
			
				
			
			Book update = bookService.update(book);

			List<Book> books = bookService.findAll();
			ModelMap rezultat2 = new ModelMap();
			rezultat2.addAttribute("books", books);
			// return "redirect:/";
			return new ModelAndView("redirect:/", rezultat2);

		}

	}
	@GetMapping(value="/Cart") 
	public ModelAndView cart(HttpServletResponse response, HttpSession session) throws IOException {
		
		User user =(User)session.getAttribute("currentUser");
		if(user == null) {
			response.sendRedirect("/LaLivret/");
		}
		
		//************ refresh cart if anything is changed
		if(session.getAttribute("cart") != null) {
		int totalPrice = 0;
		Purchase cart = (Purchase) session.getAttribute("cart");
		for(BoughtBook i : cart.getBooks()) {
			
			Book bookUpdated = bookService.findOne(i.getBookId().getID());
			i.setBookId(bookUpdated);
			i.setPrice(i.getNumberOfBooks() * i.getBookId().getPrice());
			totalPrice += i.getPrice();
		}
		cart.setPrice(totalPrice);
		System.out.println("cart after change: " + cart);
		session.setAttribute("cart", cart);
		session.setAttribute("totalPrice", totalPrice);
		}
		//***********
		
		
		ModelAndView rezultat = new ModelAndView("cart");
		
		
		LoyaltyCard card = loyaltyCardService.findByUserID(user.getID());
		if(card != null) {
			rezultat.addObject("cardPoints", card.getPointNumber());
		}
		
		if(specialDateService.isTodaySpecialDate()) {
			SpecialDate specialDate = specialDateService.findOne(LocalDate.now());
			rezultat.addObject("specialDate", specialDate);
		}
		
		
		return rezultat;
	}
	
	
	@PostMapping(value="/AddToCart")
	public void addToCart(HttpSession session, @RequestParam Long ID,@RequestParam int amount, HttpServletResponse response) throws IOException {
		
		Long purchaseId =Math.round((Math.random() * (999999 - 1 + 1) +1));
		
		Purchase cart = new Purchase();
		
		Book book = bookService.findOne(ID);
		
		book.setBooksLeft(book.getBooksLeft() - amount);
		System.out.println(book);
		bookService.update(book);
		
		List<Long> bookIds = new ArrayList<Long>();
		
		if(session.getAttribute("cart") != null) { 
			  cart = (Purchase) session.getAttribute("cart"); 
			  purchaseId = cart.getID();
			  }

		if(cart.getBooks().size() != 0) {
			for(BoughtBook i : cart.getBooks()) {
				  bookIds.add(i.getBookId().getID());
			  }
		}
		
		
		if(bookIds.contains(ID)) {
			
			for(BoughtBook i : cart.getBooks()) {
				  if(i.getBookId().getID().equals(ID)) {
					  System.out.println("i enterrrd");
					  
					  i.setNumberOfBooks(i.getNumberOfBooks() + amount);
					  i.setPrice(i.getNumberOfBooks() * i.getBookId().getPrice());
					  i.setPurchaseId(purchaseId);
				  }
			  }
			
		}
		
		else {
			BoughtBook bookInCart = new BoughtBook();//Book bookId, int numberOfBooks, int price, Long purchaseId)
			bookInCart.setID(Math.round((Math.random() * (999999 - 1 + 1) +1)));
			bookInCart.setBookId(book);
			bookInCart.setNumberOfBooks(amount);
			bookInCart.setPrice(amount * book.getPrice());
			bookInCart.setPurchaseId(purchaseId);
			
			 List<BoughtBook> booksFromPurchase = cart.getBooks();
			  booksFromPurchase.add(bookInCart);
			  cart.setBooks(booksFromPurchase);
			  
			  
			  int totalPrice = 0;
			  int numberOfBooks = 0;
			  
				/* booksInCart.add(book); */
			  
			  for(BoughtBook i : cart.getBooks()) { 
				  totalPrice += i.getPrice(); 
				  numberOfBooks += i.getNumberOfBooks();
				  }
			  
			  cart.setPrice(totalPrice);
			  cart.setNumberOfBooks(numberOfBooks);
			  session.setAttribute("totalPrice", totalPrice);
			  
		}
		  
		
//		
//		Book book = bookService.findOne(ID);
//		System.out.println(book);
//		BoughtBook bookInCart = new BoughtBook();//Book bookId, int numberOfBooks, int price, Long purchaseId)
//		bookInCart.setID(Math.round((Math.random() * (999999 - 1 + 1) +1)));
//		bookInCart.setBookId(book);
//		bookInCart.setNumberOfBooks(amount);
//		bookInCart.setPrice(amount * book.getPrice());
//		bookInCart.setPurchaseId(purchaseId);
		
		
		
		
		  cart.setUser((User)session.getAttribute("currentUser"));
		  
		  cart.setID(purchaseId);
		  
		 
//		 BoughtBook save = boughtBookService.save(bookInCart);
		  
		  
		  
//		  List<BoughtBook> booksFromPurchase = cart.getBooks();
//		  booksFromPurchase.add(bookInCart);
		  
//		  cart.setBooks(booksFromPurchase);
//		  cart.setUser((User)session.getAttribute("currentUser"));
//		  cart.setPurchaseDate(LocalDate.now());
//		  
//		  
//		  
//		  int totalPrice = 0;
//		  int numberOfBooks = 0;
//		  
//			/* booksInCart.add(book); */
//		  
//		  for(BoughtBook i : cart.getBooks()) { 
//			  totalPrice += i.getPrice(); 
//			  numberOfBooks += i.getNumberOfBooks();
//			  }
//		  
//		  cart.setPrice(totalPrice);
//		  cart.setNumberOfBooks(numberOfBooks);
//		  
//		  session.setAttribute("totalPrice", totalPrice);
		  session.setAttribute("cart", cart); 
		  System.out.println(cart); System.out.println(amount);
		
		response.sendRedirect("/LaLivret/");
		
	}
	
	@PostMapping(value="/RemoveFromCart")
	public void removeFromCart(@RequestParam Long boughtBookID, HttpSession session, HttpServletResponse response) throws IOException {

		System.out.println("id from html: " + boughtBookID);
		System.out.println("id from purchase book: ");
		
		Purchase purchase =(Purchase) session.getAttribute("cart");
		
		/*
		 * for(BoughtBook i : purchase.getBooks()) { System.out.println(i.getID());
		 * if(i.getID().equals(boughtBookID)) { BoughtBook toRemove = i; }
		 * 
		 * }
		 */
		List<BoughtBook> booksRemove = purchase.getBooks();
		booksRemove.removeIf(remove -> remove.getID().equals(boughtBookID));
		purchase.setBooks(booksRemove);
		
//		if(purchase.getBooks().contains(book) ) {
//			List<BoughtBook> booksWithout = purchase.getBooks();
//			booksWithout.remove(book);
//			purchase.setBooks(booksWithout);
//		}
		

		  int totalPrice = 0;
		  int totalBookCount = 0;
		  for(BoughtBook i : purchase.getBooks()) { 
			  totalPrice += i.getPrice(); 
			 
			  }
		  for(BoughtBook i : purchase.getBooks()) { 
			  totalBookCount += i.getNumberOfBooks(); 
			 
			  }
		  
		  
		purchase.setPrice(totalPrice);
		purchase.setNumberOfBooks(totalBookCount);
		  
		System.out.println("purchase after: " + purchase);
		
		session.setAttribute("totalPrice", totalPrice);
		session.setAttribute("cart", purchase);
		
		response.sendRedirect("/LaLivret/Cart");
		
	}
	
	@PostMapping(value="/Buy")
	public void buy(@RequestParam(required = false) Integer points,HttpSession session, HttpServletResponse response) throws IOException {
		Purchase purchase =(Purchase) session.getAttribute("cart");
		int totalPrice = (int) session.getAttribute("totalPrice");
		
		
		User user = (User) session.getAttribute("currentUser");
		
		double priceWithDiscount = totalPrice;
		
		
		if(specialDateService.isTodaySpecialDate()) {
			
			SpecialDate specialDate = specialDateService.findOne(LocalDate.now());
			double discountToday = specialDate.getDiscount();
			priceWithDiscount = totalPrice - (totalPrice * discountToday);
			purchase.setPrice((int)Math.round(priceWithDiscount));
		}
		
		else {
		
		LoyaltyCard card = loyaltyCardService.findByUserID(user.getID());
		
		if(card != null) {
		
		if(points != null && points != 0) {
			
		card.setPointNumber(card.getPointNumber() - points);
		
		System.out.println("points left on card: " + card.getPointNumber());
		double discountLeft = card.getPointNumber() * 0.05;
		System.out.println("discount left on card: " + discountLeft);
		
		card.setDiscount(discountLeft);
		loyaltyCardService.update(card);
		
		
		priceWithDiscount = totalPrice - (totalPrice * (points * 0.05));
		}
		
		
		
		System.out.println("price with discount: " + priceWithDiscount);
		}
		
		purchase.setPrice((int)Math.round(priceWithDiscount));
		System.out.println(purchase);
		
		if(card != null) {
		int pointsEarned = (int) priceWithDiscount / 1000;
		System.out.println("points earned: " + pointsEarned);
		
		card.setPointNumber(card.getPointNumber() + pointsEarned);
		card.setDiscount(card.getPointNumber() * 0.05);
		loyaltyCardService.update(card);
		}
		}
		
		
		purchaseService.save(purchase);
		
		for(BoughtBook i : purchase.getBooks()) {
			boughtBookService.save(i);
		}
		
		
		session.setAttribute("cart", null);
		session.setAttribute("totalPrice", 0);
		response.sendRedirect("/LaLivret/");
		
	}
	
	@PostMapping(value="/OrderBooks")
	public void order(HttpServletResponse response, @RequestParam Long ID, @RequestParam int amountOrder) throws IOException {
		
		Book book = bookService.findOne(ID);
		if(book != null) {
			book.setBooksLeft(book.getBooksLeft() + amountOrder);
			bookService.update(book);
		}
		
		
		response.sendRedirect("/LaLivret/BookDetails?id=" + ID);
	}
	
	@PostMapping(value="/RemoveFromWishlist")
	public void removeFromWishlist(HttpServletResponse response, @RequestParam Long ID, HttpSession session) throws IOException {
		User user = (User) session.getAttribute("currentUser");
		wishlistService.delete(user.getID(), ID);
		response.sendRedirect("/LaLivret/Users/CurrentUserDetails");
	}
	
	@PostMapping(value="/AddToWishlist") 
	public void addToWishlist(HttpServletResponse response, @RequestParam Long ID, HttpSession session) throws IOException {
		User user = (User) session.getAttribute("currentUser");
		wishlistService.save(user.getID(), ID);
		response.sendRedirect("/LaLivret");
	}

}
