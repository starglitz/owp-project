package com.ftn.OWPproject.dao;

import java.util.List;

import com.ftn.OWPproject.model.Genre;


public interface GenreDAO {

	public Genre findOne(Long id);

	public List<Genre> findAll();

	
}
