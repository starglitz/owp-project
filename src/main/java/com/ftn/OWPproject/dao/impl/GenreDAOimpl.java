package com.ftn.OWPproject.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.ftn.OWPproject.dao.GenreDAO;
import com.ftn.OWPproject.model.Genre;



@Repository
public class GenreDAOimpl implements GenreDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	
	private class GenreRowMapper implements RowMapper<Genre> {

		@Override
		public Genre mapRow(ResultSet rs, int rowNum) throws SQLException {
			int index = 1;
			Long id = rs.getLong(index++);
			String name = rs.getString(index++);
			String description = rs.getString(index++);

			Genre genre = new Genre(id, name, description);
			return genre;
		}

	}
	
	
	@Override
	public Genre findOne(Long id) {
		String sql = "SELECT id, name, description FROM genre WHERE id = ?";
		return jdbcTemplate.queryForObject(sql, new GenreRowMapper(), id);
	}

	@Override
	public List<Genre> findAll() {
		String sql = "SELECT id, name, description FROM genre";
		return jdbcTemplate.query(sql, new GenreRowMapper());
	}


}
