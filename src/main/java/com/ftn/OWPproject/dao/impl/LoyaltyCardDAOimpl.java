package com.ftn.OWPproject.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ftn.OWPproject.dao.LoyaltyCardDAO;
import com.ftn.OWPproject.dao.UserDAO;
import com.ftn.OWPproject.model.LoyaltyCard;
import com.ftn.OWPproject.model.LoyaltyCardStatus;
import com.ftn.OWPproject.model.User;

@Repository
public class LoyaltyCardDAOimpl implements LoyaltyCardDAO{

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private UserDAO userDAO;

	
private class LoyaltyCardRowCallBackHandler implements RowCallbackHandler {
		
		private Map<Long, LoyaltyCard> cards = new LinkedHashMap<>();
		
		@Override
		public void processRow(ResultSet resultSet) throws SQLException {
			int index = 1;
			Long cardId = resultSet.getLong(index++);
		    double discount = resultSet.getDouble(index++);
			Long userId = resultSet.getLong(index++);
			User user = userDAO.findOne(userId);
			int points = resultSet.getInt(index++);
			String statusStr = resultSet.getString(index++);
			LoyaltyCardStatus status = LoyaltyCardStatus.valueOf(statusStr);
			
			LoyaltyCard card = cards.get(cardId);
		
			if (card == null) {
				
			
			    card = new LoyaltyCard(cardId, discount, points, user, status);
				cards.put(card.getID(), card);
				
				
			}

			
		}
			public List<LoyaltyCard> getLoyaltyCards() {
				return new ArrayList<>(cards.values());
		}
	
	
	
}


@Override
public LoyaltyCard findOne(Long id) {
	String sql = "SELECT" + 
			"		id, discount, userid, points, status " + 
			"FROM loyaltycard " + 
			"WHERE id = ?";
	
	LoyaltyCardRowCallBackHandler rowCallbackHandler = new LoyaltyCardRowCallBackHandler();
	jdbcTemplate.query(sql, rowCallbackHandler, id);

	return rowCallbackHandler.getLoyaltyCards().get(0);
}


@Override
public List<LoyaltyCard> findAll() {
	String sql = "SELECT" + 
			"		id, discount, userid, points, status " + 
			"FROM loyaltycard ";
	LoyaltyCardRowCallBackHandler rowCallbackHandler = new LoyaltyCardRowCallBackHandler();
	jdbcTemplate.query(sql, rowCallbackHandler);

	return rowCallbackHandler.getLoyaltyCards();
}


@Override
public void save(LoyaltyCard card) {
	String sql = "INSERT INTO loyaltycard (id, discount, userid, points, status) VALUES (?, ?, ?, ?, ?)";
	jdbcTemplate.update(sql, card.getID(), card.getDiscount(), card.getUser().getID(), card.getPointNumber(), card.getStatus().toString());
	
}


@Override
public void update(LoyaltyCard card) {
	String sql = "update loyaltycard set  discount = ?, userid = ?, points = ?, status = ? where id = ?";
	jdbcTemplate.update(sql, card.getDiscount(), card.getUser().getID(), card.getPointNumber(), card.getStatus().toString(),card.getID());
	
}


@Override
public LoyaltyCard findByUserID(Long userID) {
	String sql = "SELECT" + 
			"		id, discount, userid, points, status " + 
			"FROM loyaltycard " +
			"WHERE userId = ?";
	
	LoyaltyCardRowCallBackHandler rowCallbackHandler = new LoyaltyCardRowCallBackHandler();
	jdbcTemplate.query(sql, rowCallbackHandler, userID);

	if(!rowCallbackHandler.getLoyaltyCards().isEmpty()) {
	
	return rowCallbackHandler.getLoyaltyCards().get(0);
	}
	
	return null;
}


@Transactional
@Override
public void delete(Long id) {
	String sql = "DELETE FROM loyaltycard WHERE id = ?";
	jdbcTemplate.update(sql, id);

}

}

