package com.ftn.OWPproject.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.ftn.OWPproject.dao.UserDAO;
import com.ftn.OWPproject.model.Role;
import com.ftn.OWPproject.model.User;

@Repository
public class UserDAOimpl implements UserDAO{

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private class UserRowMapper implements RowMapper<User> {

		@Override
		public User mapRow(ResultSet rs, int rowNum) throws SQLException {
			int index = 1;
			Long ID = rs.getLong(index++);
			String username = rs.getString(index++);
			String password = rs.getString(index++);
			String email = rs.getString(index++);
			String name = rs.getString(index++);
			String surname = rs.getString(index++);
			LocalDate dateOfBirth = rs.getDate(index++).toLocalDate();
			String address = rs.getString(index++);
			String contact = rs.getString(index++);
			LocalDateTime registerDate = rs.getObject(index++, LocalDateTime.class);
			String role = rs.getString(index++);
			/* Role role = Role.valueOf(roleStr); */
			boolean isBlocked = rs.getBoolean(index++);
			
			User user = new User(ID, username, password, email, name, surname,
					dateOfBirth, address, contact, registerDate, role, isBlocked);
			
			return user;
		}

	}
	
	
	@Override
	public User findOne(String username) {
		try {
			String sql = "SELECT id, username, password, email, name, surname, dateOfBirth, address,"
					+ "contact, registerDate, role, isBlocked FROM user WHERE username = ?";
			return jdbcTemplate.queryForObject(sql, new UserRowMapper(), username);
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}
	
	@Override
	public User findOne(Long id) {
		try {
			String sql = "SELECT id, username, password, email, name, surname, dateOfBirth, address,"
					+ "contact, registerDate, role, isBlocked FROM user WHERE id = ?";
			return jdbcTemplate.queryForObject(sql, new UserRowMapper(), id);
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}

	@Override
	public User findOne(String username, String password) {
		try {
			String sql = "SELECT id, username, password, email, name, surname, dateOfBirth, address,"
					+ "contact, registerDate, role, isBlocked FROM user WHERE username = ? and password = ?";
			return jdbcTemplate.queryForObject(sql, new UserRowMapper(), username, password);
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}
	
	
	@Override
	public List<User> findAll() {
		String sql = "SELECT id, username, password, email, name, surname, dateOfBirth, address,"
				+ "contact, registerDate, role, isBlocked FROM user";
		return jdbcTemplate.query(sql, new UserRowMapper());
	}

	@Override
	public void save(User user) {
		String sql = "INSERT INTO user (username, password, email, name, surname, dateOfBirth, address, contact,"
				+ "registerDate, role, isBlocked) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		jdbcTemplate.update(sql, user.getUsername(), user.getPassword(), user.getEmail(), user.getName(),
				user.getSurname(), user.getDateOfBirth(), user.getAddress(), user.getContact(), user.getRegisterDate(), 
				user.getRole(), user.isBlocked());
	}

	@Override
	public void update(User user) {
		String sql = "UPDATE user SET password = ?, email = ?, name = ?, surname = ?, dateOfBirth = ?, address = ?,"
				+ "contact = ?, registerDate = ?, role = ?, isBlocked = ?  WHERE username = ?";
		jdbcTemplate.update(sql, user.getPassword(), user.getEmail(), user.getName(), user.getSurname(),
				user.getDateOfBirth(), user.getAddress(), user.getContact(), user.getRegisterDate(), user.getRole(), user.isBlocked(),user.getUsername());
	}

	@Override
	public void delete(String username) {
		String sql = "DELETE FROM user WHERE username = ?";
		jdbcTemplate.update(sql, username);
	}

}
