package com.ftn.OWPproject.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.ftn.OWPproject.dao.BookDAO;
import com.ftn.OWPproject.dao.UserDAO;
import com.ftn.OWPproject.dao.WishlistDAO;
import com.ftn.OWPproject.model.Book;
import com.ftn.OWPproject.model.Genre;
import com.ftn.OWPproject.model.User;
import com.ftn.OWPproject.model.Wishlist;
import com.ftn.OWPproject.model.WishlistDB;

@Repository
public class WishlistDAOimpl implements WishlistDAO{

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private BookDAO bookDAO; 
	
	@Autowired
	private UserDAO userDAO;
	
	
	
	private class WishlistDBRowMapper implements RowMapper<WishlistDB> {

		private List<WishlistDB> wishlists = new ArrayList<WishlistDB>();
		
		@Override
		public WishlistDB mapRow(ResultSet rs, int rowNum) throws SQLException {
			int index = 1;
			Long userID = rs.getLong(index++);
			Long bookID = rs.getLong(index++);
			
			

			WishlistDB wishlist = new WishlistDB(userID, bookID);
			
			wishlists.add(wishlist);
			return wishlist;
		}
		
		public List<WishlistDB> getWishlistDBs() {
			return new ArrayList<>(wishlists);
	
	}
		
		
		

}
	
	@Override
	public Wishlist findByUserId(Long userID) {
		List<WishlistDB> wishlistDBs = findAllWishlistDB();
		Wishlist usersWishlist = new Wishlist();
		List<Book> books = new ArrayList<Book>();
		
		usersWishlist.setUser(userDAO.findOne(userID));
		for(WishlistDB wdb : wishlistDBs) {
			if(wdb.getUserID().equals(userID)) {
				books.add(bookDAO.findOne(wdb.getBookID()));
			}
		}
		
		usersWishlist.setBooks(books);
		return usersWishlist;
	}

	@Override
	public List<WishlistDB> findAllWishlistDB() {
		String sql = "SELECT userid, bookid FROM wishlist";
		return jdbcTemplate.query(sql, new WishlistDBRowMapper());
	}
	
	

	@Override
	public void save(Long userID, Long bookID) {
		String sql = "INSERT INTO wishlist (userid, bookid) VALUES (?, ?)";
		jdbcTemplate.update(sql, userID, bookID);
	}

	@Override
	public void delete(Long userID, Long bookID) {
		String sql = "DELETE FROM wishlist WHERE userid = ? and bookid = ?";
		jdbcTemplate.update(sql, userID, bookID);
	}

	

}


