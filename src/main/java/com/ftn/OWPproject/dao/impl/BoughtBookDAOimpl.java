package com.ftn.OWPproject.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ftn.OWPproject.dao.BookDAO;
import com.ftn.OWPproject.dao.BoughtBookDAO;
import com.ftn.OWPproject.dao.GenreDAO;
import com.ftn.OWPproject.dao.PurchaseDAO;
import com.ftn.OWPproject.model.Book;
import com.ftn.OWPproject.model.BoughtBook;
import com.ftn.OWPproject.model.Cover;
import com.ftn.OWPproject.model.Genre;
import com.ftn.OWPproject.model.Writing;

@Repository
public class BoughtBookDAOimpl implements BoughtBookDAO{

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	
	@Autowired
	private BookDAO bookDAO; 
	
	
	
	
	private class BoughtBookRowCallBackHandler implements RowCallbackHandler {
		
		private Map<Long, Book> books = new LinkedHashMap<>();
		private Map<Long, BoughtBook> boughtBooks = new LinkedHashMap<>();
		
		@Override
		public void processRow(ResultSet resultSet) throws SQLException {
			int index = 1;
			Long boughtBookId = resultSet.getLong(index++);
			Long bookId = resultSet.getLong(index++);
			int bookNumber = resultSet.getInt(index++); 
			int boughtBookPrice = resultSet.getInt(index++); 
			Long purchaseId = resultSet.getLong(index++);
			
			
			Book book = bookDAO.findOne(bookId);
	
			
			
			BoughtBook boughtbook = boughtBooks.get(boughtBookId);
			if (boughtbook == null) {
		
				
				boughtbook = new BoughtBook(boughtBookId, book, bookNumber, boughtBookPrice, purchaseId);
				boughtBooks.put(boughtbook.getID(), boughtbook);
				
				
			}

			
		}
			public List<BoughtBook> getBoughtBooks() {
				return new ArrayList<>(boughtBooks.values());
		}
	
	
	
	}




	@Override
	public BoughtBook findOne(Long id) {
		String sql = "SELECT" + 
				"		id, bookid, booknumber,price,purchaseId " + 
				"FROM boughtbook " +
				"WHERE id = ?";
		
		BoughtBookRowCallBackHandler rowCallbackHandler = new BoughtBookRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler, id);

		return rowCallbackHandler.getBoughtBooks().get(0);
	}


	@Override
	public List<BoughtBook> findAll() {
		String sql = "SELECT" + 
				"		id, bookid, booknumber,price,purchaseId " + 
				"FROM boughtbook";
		BoughtBookRowCallBackHandler rowCallbackHandler = new BoughtBookRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler);

		return rowCallbackHandler.getBoughtBooks();
	}

	@Transactional
	@Override
	public void save(BoughtBook book) {
		
		String sql = "INSERT INTO boughtbook (bookid, booknumber, price, purchaseid)"
				+ " VALUES (?, ?, ?, ?)";
		
		jdbcTemplate.update(sql, book.getBookId().getID(), book.getNumberOfBooks(),book.getPrice(), book.getPurchase());
		
		
		
		
		
		
	}
	

	
	


	@Override
	public void update(BoughtBook book) {
		String sql = "update boughtbook set bookid = ?, booknumber = ? , price = ?, purchaseid = ? where id = ?"
				+ " VALUES (?, ?, ?, ?)";
		
		jdbcTemplate.update(sql, book.getBookId().getID(), book.getNumberOfBooks(), book.getPrice(), book.getPurchase(),
				book.getID());
	}
	
	
	public List<BoughtBook> findBooksByPurchaseId(Long purchaseId) {
		String sql = "SELECT" + 
				"		id, bookid, booknumber,price,purchaseId " + 
				"FROM boughtbook " +
				"WHERE purchaseId = ?";
		
		BoughtBookRowCallBackHandler rowCallbackHandler = new BoughtBookRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler, purchaseId);

		return rowCallbackHandler.getBoughtBooks();
		
		
	}
	
	
}
