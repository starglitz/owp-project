package com.ftn.OWPproject.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Repository;

import com.ftn.OWPproject.dao.BookDAO;
import com.ftn.OWPproject.dao.BoughtBookDAO;
import com.ftn.OWPproject.dao.PurchaseDAO;
import com.ftn.OWPproject.dao.UserDAO;
import com.ftn.OWPproject.model.Book;
import com.ftn.OWPproject.model.BoughtBook;
import com.ftn.OWPproject.model.Purchase;
import com.ftn.OWPproject.model.User;

@Repository
public class PurchaseDAOimpl implements PurchaseDAO {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private BoughtBookDAO boughtBookDAO; 
	
	@Autowired
	private UserDAO userDAO;
	
	private class PurchaseRowCallBackHandler implements RowCallbackHandler {
		
		private Map<Long, Purchase> purchases = new LinkedHashMap<>();
		
		@Override
		public void processRow(ResultSet resultSet) throws SQLException {
			int index = 1;
			Long purchaseId = resultSet.getLong(index++);
			int price = resultSet.getInt(index++);
			LocalDate date = resultSet.getDate(index++).toLocalDate();
			Long userId = resultSet.getLong(index++);
			User user = userDAO.findOne(userId);
			int bookCount = resultSet.getInt(index++);
			  
			
			List<BoughtBook> boughtBooks = boughtBookDAO.findBooksByPurchaseId(purchaseId);
			
			
			
			Purchase purchase = purchases.get(purchaseId);
		
			if (purchase == null) {
				
			
				purchase = new Purchase(purchaseId,date, user, bookCount,price, boughtBooks);
				purchases.put(purchase.getID(), purchase);
				
				
			}

			
		}
			public List<Purchase> getPurchases() {
				return new ArrayList<>(purchases.values());
		}
	
	
	
	}

		@Override
		public Purchase findOne(Long id) {
			String sql = "SELECT" + 
					"		id, price, date, userid, bookcount " + 
					"FROM purchase " + 
					"WHERE id = ?";
			
			PurchaseRowCallBackHandler rowCallbackHandler = new PurchaseRowCallBackHandler();
			jdbcTemplate.query(sql, rowCallbackHandler, id);

			return rowCallbackHandler.getPurchases().get(0);
		}
		
		@Override
		public List<Purchase> findAll() {
			String sql = "SELECT" + 
					"		id, price, date, userid, bookcount " + 
					"FROM purchase";
			PurchaseRowCallBackHandler rowCallbackHandler = new PurchaseRowCallBackHandler();
			jdbcTemplate.query(sql, rowCallbackHandler);

			return rowCallbackHandler.getPurchases();
		}
		
		@Override
		public void save(Purchase purchase) {
			String sql = "INSERT INTO purchase (id, price, date, userid, bookcount) VALUES (?, ?, ?, ?, ?)";
			jdbcTemplate.update(sql, purchase.getID(), purchase.getPrice(), purchase.getPurchaseDate(), 
					purchase.getUser().getID(), purchase.getNumberOfBooks());
			
		}

		@Override
		public void update(Purchase purchase) {
			String sql = "update purchase set  price = ? , date = ?, userid = ?, bookcount = ? where id = ?"
					+ " VALUES (?, ?, ?, ?)";
			
			jdbcTemplate.update(sql, purchase.getPrice(), purchase.getPurchaseDate(), 
					purchase.getUser().getID(), purchase.getNumberOfBooks(), purchase.getID());
			
		}
		@Override
		public List<Purchase> findPurchasesByUser(Long userID) {
			String sql = "SELECT" + 
					"		id, price, date, userid, bookcount " + 
					"FROM purchase " + 
					"WHERE userid = ?" +
					" order by date desc";
			
			PurchaseRowCallBackHandler rowCallbackHandler = new PurchaseRowCallBackHandler();
			jdbcTemplate.query(sql, rowCallbackHandler, userID);

			return rowCallbackHandler.getPurchases();
		}
	
}
