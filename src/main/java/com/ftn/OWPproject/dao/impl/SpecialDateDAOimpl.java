package com.ftn.OWPproject.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Repository;

import com.ftn.OWPproject.dao.SpecialDateDAO;
import com.ftn.OWPproject.model.SpecialDate;

@Repository
public class SpecialDateDAOimpl implements SpecialDateDAO{

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
private class SpecialDateRowCallBackHandler implements RowCallbackHandler {
		
		private Map<Long, SpecialDate> specialDates = new LinkedHashMap<>();
		
		@Override
		public void processRow(ResultSet resultSet) throws SQLException {
			int index = 1;
			Long id = resultSet.getLong(index++);
		    LocalDate date = resultSet.getDate(index++).toLocalDate();
			double discount = resultSet.getDouble(index++);
			
			
			SpecialDate specialDate = specialDates.get(id);
		
			if (specialDate == null) {
				
			
			    specialDate = new SpecialDate(id, date, discount);
				specialDates.put(specialDate.getID(), specialDate);
				
				
			}

			
		}
			public List<SpecialDate> getSpecialDates() {
				return new ArrayList<>(specialDates.values());
		}
	
	
	
}
	
	@Override
	public SpecialDate findOne(LocalDate date) {
		String sql = "SELECT" + 
				"		id, specialdate, discount " + 
				"FROM specialdate " + 
				"WHERE specialdate = ?";
		
		SpecialDateRowCallBackHandler rowCallbackHandler = new SpecialDateRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler, date);

		return rowCallbackHandler.getSpecialDates().get(0);
	}

	@Override
	public List<SpecialDate> findAll() {
		String sql = "SELECT" + 
				"		id, specialdate, discount " + 
				"FROM specialdate ";
		SpecialDateRowCallBackHandler rowCallbackHandler = new SpecialDateRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler);

		return rowCallbackHandler.getSpecialDates();
	}

	@Override
	public void save(SpecialDate date) {
		String sql = "INSERT INTO specialdate (id, specialdate, discount) VALUES (?, ?, ?)";
		jdbcTemplate.update(sql, date.getID(), date.getDate(), date.getDiscount());
		
	}

	@Override
	public boolean isTodaySpecialDate() {
		LocalDate today = LocalDate.now();
		List<SpecialDate> allDates = findAll();
		for(SpecialDate i : allDates) {
			if(today.equals(i.getDate())) {
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean doesDateAlreadyExist(LocalDate date) {
		List<SpecialDate> allDates = findAll();
		for(SpecialDate i : allDates) {
			if(date.equals(i.getDate())) {
				return true;
			}
		}
		return false;
	}

	
	
}
