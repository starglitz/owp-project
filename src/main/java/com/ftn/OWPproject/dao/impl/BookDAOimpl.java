package com.ftn.OWPproject.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ftn.OWPproject.dao.BookDAO;
import com.ftn.OWPproject.dao.CommentDAO;
import com.ftn.OWPproject.dao.GenreDAO;
import com.ftn.OWPproject.model.Book;
import com.ftn.OWPproject.model.Comment;
import com.ftn.OWPproject.model.Cover;
import com.ftn.OWPproject.model.Genre;
import com.ftn.OWPproject.model.Writing;
import com.ftn.OWPproject.service.CommentService;



@Repository
public class BookDAOimpl implements BookDAO{

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private GenreDAO genreDAO; 
	
	@Autowired
	private CommentDAO commentDAO;
	
	
	private class BookGenreRowCallBackHandler implements RowCallbackHandler {

		private Map<Long, Book> books = new LinkedHashMap<>();
		
		@Override
		public void processRow(ResultSet resultSet) throws SQLException {
			int index = 1;
			Long bookId = resultSet.getLong(index++);
			String bookTitle = resultSet.getString(index++); 
			Long bookIsbn = resultSet.getLong(index++); 
			String bookPublisher = resultSet.getString(index++);  
			String bookAuthor = resultSet.getString(index++); 
			int bookPublishYear = resultSet.getInt(index++); 
			String bookDescription = resultSet.getString(index++); 
			String bookPicturepath = resultSet.getString(index++); 
			int bookPrice = resultSet.getInt(index++); 
			int bookPages = resultSet.getInt(index++);
			String bookCover = resultSet.getString(index++);
			Cover bookCoverEnum = Cover.valueOf(bookCover);
			String bookWriting = resultSet.getString(index++); 
			Writing bookWritingEnum = Writing.valueOf(bookWriting);
			String bookLanguage = resultSet.getString(index++); 
			int booksLeft = resultSet.getInt(index++);
		
		//implement rating
			double rating = 0;
			
			int finalNumber = 0;
			int count = 0;
			
			List<Comment> commentsOnBook = commentDAO.findByBook(bookId);
			if(commentsOnBook.size() != 0) {
				for(Comment comment : commentsOnBook) {
					count += 1;
					finalNumber += comment.getRating();
				}
				
				rating = finalNumber / count;
			}
			
			
			
			Book book = books.get(bookId);
			if (book == null) {
//				int ID,String name, String iSBN, String publisher,String publishYear, String author, String picturePath, int price,
//				int numberOfPages, Cover cover, Writing writing, String language, double rating,  ArrayList<Genre> genres) {
		//	}
				book = new Book(bookId, bookTitle, bookIsbn, bookPublisher ,bookPublishYear, bookAuthor, 
						"images/" + bookPicturepath, bookPrice, bookPages,bookCoverEnum, bookWritingEnum, bookLanguage, rating, bookDescription, booksLeft);
				books.put(book.getID(), book); // dodavanje u kolekciju
			}

			Long genreID = resultSet.getLong(index++);
			String genreName = resultSet.getString(index++);
			String genreDescription = resultSet.getString(index++);
			if(genreID != 0) {
			
			Genre genre = new Genre(genreID, genreName, genreDescription);
			book.getGenres().add(genre);
			}
		}
			public List<Book> getBooks() {
				return new ArrayList<>(books.values());
		}
	
}

	
	@Override
	public List<Book> findAll() {
		String sql = 
//				Long genreID = resultSet.getLong(index++);
//		String genreName = resultSet.getString(index++);
//		String genreDescription = resultSet.getString(index++);
//				 bookTitle, bookIsbn, bookPublisher ,bookPublishYear, bookAuthor, 
//					bookPicturepath, bookPrice, bookPages,bookCoverEnum, bookWritingEnum, bookLanguage, bookRating);
				"SELECT b.id, b.title,  b.isbn, b.publisher, b.author, b.publishyear, b.description,"
				+ " b.picturepath, b.price, b.pages, b.cover, b.writing, b.language,b.booksLeft, g.id, g.name,"
				+ " g.description FROM book b " +
				"LEFT JOIN bookgenre bg ON bg.bookId = b.id " + 
				"LEFT JOIN genre g ON bg.genreId = g.id " + 
				"ORDER BY b.id";

		BookGenreRowCallBackHandler rowCallbackHandler = new BookGenreRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler);

		return rowCallbackHandler.getBooks();
	}

	@Override
	public Book findOne(Long id) {
		String sql = 
				"SELECT b.id, b.title,  b.isbn, b.publisher, b.author, b.publishyear, b.description,"
				+ " b.picturepath, b.price, b.pages, b.cover, b.writing, b.language,b.booksLeft,"
				+ " g.id, g.name, g.description FROM book b " +
				"LEFT JOIN bookGenre bg ON bg.bookId = b.id " + 
				"LEFT JOIN genre g ON bg.genreId = g.id " + 
				"WHERE b.id = ? " + 
				"ORDER BY b.id";

		BookGenreRowCallBackHandler rowCallbackHandler = new BookGenreRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler, id);

		return rowCallbackHandler.getBooks().get(0);
	}
	
	@Transactional
	@Override
	public int save(Book book) {
		PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
			
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				String sql = "INSERT INTO book (title, isbn, publisher, author, publishyear, description,"
						+ " picturepath, price, pages, cover, writing, language, booksLeft)"
						+ " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

				PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				int index = 1;
				preparedStatement.setString(index++, book.getTitle());
				preparedStatement.setLong(index++, book.getISBN());
				preparedStatement.setString(index++, book.getPublisher());
				preparedStatement.setString(index++, book.getAuthor());
				preparedStatement.setInt(index++, book.getPublishYear());
				preparedStatement.setString(index++, book.getDescription());
				preparedStatement.setString(index++, book.getPicturePath());
				preparedStatement.setInt(index++, book.getPrice());
				preparedStatement.setInt(index++, book.getNumberOfPages());
				preparedStatement.setString(index++, book.getCover().toString());
				preparedStatement.setString(index++, book.getWriting().toString());
				preparedStatement.setString(index++, book.getLanguage());
				preparedStatement.setDouble(index++, book.getBooksLeft());
				

				
				
				return preparedStatement;
			}

		};
		GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
		boolean uspeh = jdbcTemplate.update(preparedStatementCreator, keyHolder) == 1;
		if (uspeh) {
			String sql = "INSERT INTO bookGenre (bookId, genreId) VALUES (?, ?)";
			for (Genre itGenre: book.getGenres()) {	
				uspeh = uspeh && jdbcTemplate.update(sql, keyHolder.getKey(), itGenre.getID()) == 1;
			}
		}
		return uspeh?1:0;
	}

	@Override
	public int update(Book book) {
		System.out.println(book);
		
		if(!book.getGenres().isEmpty()) {
		String sql = "DELETE FROM BookGenre WHERE bookId = ?";
		jdbcTemplate.update(sql, book.getID());
	
		boolean uspeh = true; 
		
		sql = "INSERT INTO bookGenre (bookId, genreId) VALUES (?, ?)";
		for (Genre itGenre: book.getGenres()) {	
			uspeh = uspeh &&  jdbcTemplate.update(sql, book.getID(), itGenre.getID()) == 1;
		}
		}
		
	

		String sql = "UPDATE book SET title = ?, isbn = ?, publisher = ?, author = ?, publishyear = ?, "
				+ "description = ?, picturepath = ?, price = ?, pages = ?, cover = ?,"
				+ " writing = ?, language = ?, booksLeft = ? WHERE id = ?";	

		
		String rtrim = book.getPicturePath().replaceAll("images/", "");
		
		boolean uspeh =  jdbcTemplate.update(sql, book.getTitle(), book.getISBN(),
				book.getPublisher(), book.getAuthor(), book.getPublishYear(), book.getDescription(),  rtrim, 
				book.getPrice(), book.getNumberOfPages(), book.getCover().toString(), book.getWriting().toString(), book.getLanguage()
				,book.getBooksLeft(), book.getID()) == 1;
		
		
		return uspeh?1:0;
	}

	
	
	

	
	@Transactional
	@Override
	public int delete(Long id) {
		String sql = "DELETE FROM bookGenre WHERE bookId = ?";
		jdbcTemplate.update(sql, id);

		sql = "DELETE FROM book WHERE id = ?";
		return jdbcTemplate.update(sql, id);
	}
	
	private class BookRowMapper implements RowMapper<Book> {

		@Override
		public Book mapRow(ResultSet rs, int rowNum) throws SQLException {
			int index = 1;
			Long bookId = rs.getLong(index++);
			String bookTitle = rs.getString(index++); 
			Long bookIsbn = rs.getLong(index++); 
			String bookPublisher = rs.getString(index++);  
			String bookAuthor = rs.getString(index++); 
			int bookPublishYear = rs.getInt(index++); 
			String bookDescription = rs.getString(index++); 
			String bookPicturepath = rs.getString(index++); 
			int bookPrice = rs.getInt(index++); 
			int bookPages = rs.getInt(index++);
			String bookCover = rs.getString(index++);
			Cover bookCoverEnum = Cover.valueOf(bookCover);
			String bookWriting = rs.getString(index++); 
			Writing bookWritingEnum = Writing.valueOf(bookWriting);
			String bookLanguage = rs.getString(index++); 
			int booksLeft = rs.getInt(index++);
			
			//implement rating
			double rating = 0;
			
			int finalNumber = 0;
			int count = 0;
			
			List<Comment> commentsOnBook = commentDAO.findByBook(bookId);
			if(commentsOnBook.size() != 0) {
				for(Comment comment : commentsOnBook) {
					count += 1;
					finalNumber += comment.getRating();
				}
				
				rating = finalNumber / count;
			}

			Book book = new Book(bookId, bookTitle, bookIsbn, bookPublisher ,bookPublishYear, bookAuthor, 
					"images/" + bookPicturepath, bookPrice, bookPages,bookCoverEnum, bookWritingEnum, bookLanguage,
					rating, bookDescription, booksLeft);
			return book;
		}

	}



	@Override
	public List<Book> find(String name, Integer minPrice, Integer maxPrice, String author, String language, 
			Long genreId, Long ISBN) {
		ArrayList<Object> listaArgumenata = new ArrayList<Object>();
		
		String sql = "SELECT b.id, b.title, b.price, b.author, b.language, b.ISBN FROM book b "; 
		
		StringBuffer whereSql = new StringBuffer(" WHERE ");
		boolean imaArgumenata = false;
		

		
		if(name!=null) {
			name = "%" + name + "%";
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("b.title LIKE ?");
			imaArgumenata = true;
			listaArgumenata.add(name);
		}
		
		if(author!=null) {
			author = "%" + author + "%";
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("b.language LIKE ?");
			imaArgumenata = true;
			listaArgumenata.add(author);
		}
		
		if(language!=null) {
			language = "%" + language + "%";
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("b.title LIKE ?");
			imaArgumenata = true;
			listaArgumenata.add(language);
		}

		if(minPrice!=null) {
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("b.price >= ?");
			imaArgumenata = true;
			listaArgumenata.add(minPrice);
		}
		
		if(maxPrice!=null) {
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("b.price <= ?");
			imaArgumenata = true;
			listaArgumenata.add(maxPrice);
		}
		
		if(ISBN!=null) {
			//name = "%" + name + "%";
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("b.ISBN = ?");
			imaArgumenata = true;
			listaArgumenata.add(ISBN);
		}
		
		if(imaArgumenata)
			sql=sql + whereSql.toString()+" ORDER BY b.id";
		else
			sql=sql + " ORDER BY b.id";
		
//		if(imaArgumenata)
//			sql=sql + whereSql.toString()+" ORDER BY b.? ?";

		
		System.out.println(sql);
		
		List<Book> books = jdbcTemplate.query(sql, listaArgumenata.toArray(), new BookRowMapper());
		for (Book book : books) {
			
			book.setGenres(findBookGenre(book.getID(), null));
		}
		
		if(genreId!=null)
			for (Iterator iterator = books.iterator(); iterator.hasNext();) {
				Book book = (Book) iterator.next();
				boolean zaBrisanje = true;
				for (Genre genre : book.getGenres()) {
					if(genre.getID() == genreId) {
						zaBrisanje = false;
						break;
					}
				}
				if(zaBrisanje)
					iterator.remove();
			}
		
		return books;
	}
	
	private class BookGenreRowMapper implements RowMapper<Long []> {

		@Override
		public Long [] mapRow(ResultSet rs, int rowNum) throws SQLException {
			int index = 1;
			Long bookId = rs.getLong(index++);
			Long genreId = rs.getLong(index++);

			Long [] bookGenre = {bookId, genreId};
			return bookGenre;
		}
	}


	private List<Genre> findBookGenre(Long bookId, Long genreId) {
		
		List<Genre> bookGenres = new ArrayList<Genre>();
		
		ArrayList<Object> listaArgumenata = new ArrayList<Object>();
		
		String sql = 
				"SELECT bg.bookId, bg.genreId FROM bookGenre bg ";
		
		StringBuffer whereSql = new StringBuffer(" WHERE ");
		boolean imaArgumenata = false;
		
		if(bookId!=null) {
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("bg.bookId = ?");
			imaArgumenata = true;
			listaArgumenata.add(bookId);
		}
		
		if(genreId!=null) {
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("bg.genreId = ?");
			imaArgumenata = true;
			listaArgumenata.add(genreId);
		}

		if(imaArgumenata)
			sql=sql + whereSql.toString()+" ORDER BY bg.bookId";
		else
			sql=sql + " ORDER BY bg.bookId";
		System.out.println(sql);
		
		List<Long[]> genreBooks = jdbcTemplate.query(sql, listaArgumenata.toArray(), new BookGenreRowMapper()); 
	
		for (Long[] bg : genreBooks) {
			bookGenres.add(genreDAO.findOne(bg[1]));
		}
		return bookGenres;
	}
	}

