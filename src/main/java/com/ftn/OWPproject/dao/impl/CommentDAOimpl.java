package com.ftn.OWPproject.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Repository;

import com.ftn.OWPproject.dao.BookDAO;
import com.ftn.OWPproject.dao.CommentDAO;
import com.ftn.OWPproject.dao.UserDAO;
import com.ftn.OWPproject.model.Book;
import com.ftn.OWPproject.model.Comment;
import com.ftn.OWPproject.model.CommentStatus;
import com.ftn.OWPproject.model.User;

@Repository
public class CommentDAOimpl implements CommentDAO{

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	
	@Autowired
	private UserDAO userDAO;
	
	
	
private class CommentRowCallBackHandler implements RowCallbackHandler {
		
		private Map<Long, Comment> comments = new LinkedHashMap<>();
		
		@Override
		public void processRow(ResultSet resultSet) throws SQLException {
			int index = 1;
			Long commentID = resultSet.getLong(index++);
		    int rating = resultSet.getInt(index++);
		    LocalDate date = resultSet.getDate(index++).toLocalDate();
			Long userId = resultSet.getLong(index++);
			User user = userDAO.findOne(userId);
			Long bookId = resultSet.getLong(index++);
			String statusStr = resultSet.getString(index++);
			CommentStatus status = CommentStatus.valueOf(statusStr);		
			Comment comment = comments.get(commentID);
			String description = resultSet.getString(index++);
		
			if (comment == null) {
				
				comment = new Comment(commentID, description, rating, date, user, bookId, status);
			    
				comments.put(comment.getID(), comment);
					
			}
			
		}
			public List<Comment> getComments() {
				return new ArrayList<>(comments.values());
		}
	
}
	
	@Override
	public Comment findOne(Long id) {
		String sql = "SELECT" + 
				"		id, rating, date, userid, bookid, status, description " + 
				"FROM comment " + 
				"WHERE id = ?";
		
		CommentRowCallBackHandler rowCallbackHandler = new CommentRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler, id);

		return rowCallbackHandler.getComments().get(0);
	}

	@Override
	public List<Comment> findAll() {
		String sql = "SELECT" + 
				"		id, rating, date, userid, bookid, status, description " + 
				"FROM comment";
		CommentRowCallBackHandler rowCallbackHandler = new CommentRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler);

		return rowCallbackHandler.getComments();
	}

	@Override
	public void save(Comment comment) {
		String sql = "INSERT INTO comment (id, rating, date, userid, bookid, status, description) VALUES (?, ?, ?, ?, ?, ?, ?)";
		jdbcTemplate.update(sql, comment.getID(), comment.getRating(), comment.getDate(), comment.getUser().getID(),
			comment.getBook(), comment.getStatus().toString(), comment.getDescription());
	}

	@Override
	public void update(Comment comment) {
		String sql = "update comment set  rating = ?, date = ?, userid = ?, bookid = ?, status = ?, description = ? where id = ?";
		jdbcTemplate.update(sql, comment.getRating(), comment.getDate(), comment.getUser().getID(),
				comment.getBook(), comment.getStatus().toString(), comment.getDescription(), comment.getID());
		
	}

	@Override
	public void delete(Long id) {
		String sql = "DELETE FROM comment WHERE id = ?";
		jdbcTemplate.update(sql, id);
		
	}

	@Override
	public List<Comment> findByUser(Long userID) {
		String sql = "SELECT" + 
				"		id, rating, date, userid, bookid, status, description " + 
				"FROM comment " + 
				"WHERE userid = ?";
		
		CommentRowCallBackHandler rowCallbackHandler = new CommentRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler, userID);

		return rowCallbackHandler.getComments();
	}
	
	
	@Override 
	public List<Comment> findAllApproved() {
		List<Comment> allComments = findAll();
		List<Comment> approved = new ArrayList<Comment>();
		for(Comment comment : allComments) {
			if(comment.getStatus().equals(CommentStatus.APPROVED)) {
				approved.add(comment);
			}
		}
		return approved;
	}
	
	@Override
	public List<Comment> findAllPending() {
		List<Comment> allComments = findAll();
		List<Comment> pending = new ArrayList<Comment>();
		for(Comment comment : allComments) {
			if(comment.getStatus().equals(CommentStatus.PENDING)) {
				pending.add(comment);
			}
		}
		return pending;
	}
	
	public List<Comment> findByUserAndBook(Long userID, Long BookID) {
		String sql = "SELECT" + 
				"		id, rating, date, userid, bookid, status, description " + 
				"FROM comment " + 
				"WHERE userid = ? and bookid = ?";
		
		CommentRowCallBackHandler rowCallbackHandler = new CommentRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler, userID, BookID);
		if(rowCallbackHandler.getComments() != null && rowCallbackHandler.getComments().size() != 0) {
		return rowCallbackHandler.getComments();
		}
		else {
			return null;
		}
	}
	
	
	public List<Comment> findByBook(Long bookID) {
		String sql = "SELECT" + 
				"		id, rating, date, userid, bookid, status, description " + 
				"FROM comment " + 
				"WHERE bookid = ? and status='APPROVED'";
		
		CommentRowCallBackHandler rowCallbackHandler = new CommentRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler, bookID);

		return rowCallbackHandler.getComments();
	}
	
	

}
