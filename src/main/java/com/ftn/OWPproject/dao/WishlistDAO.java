package com.ftn.OWPproject.dao;

import java.util.List;

import com.ftn.OWPproject.model.Book;
import com.ftn.OWPproject.model.Wishlist;
import com.ftn.OWPproject.model.WishlistDB;

public interface WishlistDAO {

	public Wishlist findByUserId(Long userID);

	public void save(Long userID, Long bookID);

	public void delete(Long userID, Long bookID);
	
	public List<WishlistDB> findAllWishlistDB();
	
}
