package com.ftn.OWPproject.dao;


import java.util.List;


import com.ftn.OWPproject.model.User;

public interface UserDAO {

	public User findOne(String username);
	
	public User findOne(Long id);
	
	public User findOne(String username, String password);

	public List<User> findAll();

	public void save(User user);

	public void update(User user);

	public void delete(String username);
	
	/*
	 * public List<User> find(String username, String password, String email, String
	 * name, String surname, LocalDateTime registerDate, Role role);
	 */
	
}
