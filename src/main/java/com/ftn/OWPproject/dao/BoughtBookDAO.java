package com.ftn.OWPproject.dao;

import java.util.List;

import com.ftn.OWPproject.model.BoughtBook;


public interface BoughtBookDAO {

	public BoughtBook findOne(Long id);

	public List<BoughtBook> findAll();

	public void save(BoughtBook book);

	public void update(BoughtBook book);
	
	public List<BoughtBook> findBooksByPurchaseId(Long purchaseId);
	

	/*
	 * public int delete(int id);
	 * 
	 * public List<BoughtBook> find(int numberOfBooks, int price, int purchaseId);
	 */
	
}
