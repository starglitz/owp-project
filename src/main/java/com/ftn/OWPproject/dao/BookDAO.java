package com.ftn.OWPproject.dao;

import java.util.List;

import com.ftn.OWPproject.model.Book;
import com.ftn.OWPproject.model.Cover;
import com.ftn.OWPproject.model.Writing;


public interface BookDAO {
	
	public Book findOne(Long id);

	public List<Book> findAll();

	public int save(Book book);

	public int update(Book book);

	public int delete(Long id);
	
	public List<Book> find(String name, Integer minPrice, Integer maxPrice,String author,String language, Long genreId, Long ISBN);


	
}


