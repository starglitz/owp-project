package com.ftn.OWPproject.dao;

import java.time.LocalDateTime;
import java.util.List;

import com.ftn.OWPproject.model.Purchase;


public interface PurchaseDAO {

	public Purchase findOne(Long id);

	public List<Purchase> findAll();

	public void save(Purchase purchase);

	
	  public void update(Purchase purchase);
	 /* 
	 * public int delete(int id);
	 * 
	 * public List<Purchase> find(LocalDateTime purchaseDate, int userId, int
	 * numberOfBooks);
	 */
	public List<Purchase> findPurchasesByUser(Long userID);
}
