package com.ftn.OWPproject.dao;

import java.util.List;

import com.ftn.OWPproject.model.LoyaltyCard;



public interface LoyaltyCardDAO {

	public LoyaltyCard findOne(Long id);

	public List<LoyaltyCard> findAll();

	public void save(LoyaltyCard card);

	public void update(LoyaltyCard card);
	
	public LoyaltyCard findByUserID(Long userID);
	
	public void delete(Long id);

//	public int delete(int id);
//	
//	public List<LoyaltyCard> find(double discount, int pointNumber);
	
	
}
