package com.ftn.OWPproject.model;

public class Genre {

	private Long ID;
	private String name;
	private String description;
	
	public Genre(String name, String description) {
		super();
		this.name = name;
		this.description = description;
	}
	
	public Genre(Long ID,String name, String description) {
		super();
		this.ID = ID;
		this.name = name;
		this.description = description;
	}
	
	

	public Long getID() {
		return ID;
	}

	public void setID(Long iD) {
		ID = iD;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public boolean equals(Object obj) {
		if(this == obj) 
			return true;
		if(obj == null)
			return false;
		if(getClass() != obj.getClass())
			return false;
		Genre other = (Genre) obj;
		if(ID == null) {
			if(other.ID != null) 
				return false;
		}
		else if (!ID.equals(other.ID)) 
			return false;
		return true;
	}
	
	
	@Override
	public String toString() {
		return "Genre [ID=" + ID + ", name=" + name + ", description=" + description + "]";
	}

	
	
	
	
}
