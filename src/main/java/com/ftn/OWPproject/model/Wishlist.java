package com.ftn.OWPproject.model;

import java.util.ArrayList;
import java.util.List;

public class Wishlist {

	private User user;
	private List<Book> books = new ArrayList<Book>();
	
	public Wishlist() {
		this.user = new User();
		this.books = new ArrayList<Book>();
	}
	
	public Wishlist(User user, List<Book> books) {
		super();
		this.user = user;
		this.books = books;
	}
	
	public Wishlist(User user) {
		super();
		this.user = user;
	}
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public List<Book> getBooks() {
		return books;
	}
	public void setBooks(List<Book> books) {
		this.books = books;
	}

	@Override
	public String toString() {
		return "Wishlist [user=" + user + ", books=" + books + "]";
	}
	
	
	
}
