package com.ftn.OWPproject.model;

public class WishlistDB {

	private Long userID;
	private Long bookID;
	
	
	public WishlistDB(Long userID, Long bookID) {
		super();
		this.userID = userID;
		this.bookID = bookID;
	}


	public Long getUserID() {
		return userID;
	}


	public void setUserID(Long userID) {
		this.userID = userID;
	}


	public Long getBookID() {
		return bookID;
	}


	public void setBookID(Long bookID) {
		this.bookID = bookID;
	}
	
	
	
}
