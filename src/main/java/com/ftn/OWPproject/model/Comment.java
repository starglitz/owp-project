package com.ftn.OWPproject.model;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class Comment {

	private Long ID;
	
	@NotBlank(message = "You need to enter a comment")
	private String description;
	
	@NotNull(message = "You need to choose rating")
	private int rating;
	private LocalDate date;
	private User user;
	private Long book;
	private CommentStatus status;
	
	public Comment(Long iD, String description, int rating, LocalDate date, User user, Long book,
			CommentStatus status) {
		super();
		ID = iD;
		this.description = description;
		this.rating = rating;
		this.date = date;
		this.user = user;
		this.book = book;
		this.status = status;
	}

	public Comment(String description, int rating, LocalDate date, User user, Long book, CommentStatus status) {
		super();
		this.description = description;
		this.rating = rating;
		this.date = date;
		this.user = user;
		this.book = book;
		this.status = status;
	}

	public Long getID() {
		return ID;
	}

	public void setID(Long iD) {
		ID = iD;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Long getBook() {
		return book;
	}

	public void setBook(Long book) {
		this.book = book;
	}

	public CommentStatus getStatus() {
		return status;
	}

	public void setStatus(CommentStatus status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "Comment [ID=" + ID + ", description=" + description + ", rating=" + rating + ", date=" + date
				+ ", user=" + user + ", book=" + book + ", status=" + status + "]";
	}

	

	
	
	
	
}
