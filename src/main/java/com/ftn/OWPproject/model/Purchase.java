package com.ftn.OWPproject.model;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Purchase {

	private Long ID;
	private LocalDate purchaseDate;
	private User user;
	private int numberOfBooks;
	private int price;
	private List<BoughtBook> books = new ArrayList<BoughtBook>();
	
	public Purchase(Long iD, LocalDate purchaseDate, User user, int numberOfBooks, int price, List<BoughtBook> books) {
		super();
		ID = iD;
		this.purchaseDate = purchaseDate;
		this.user = user;
		this.numberOfBooks = numberOfBooks;
		this.price = price;
		this.books = books;
	}

	public Purchase(LocalDate purchaseDate, User user, int numberOfBooks,int price, List<BoughtBook> books) {
		super();
		this.purchaseDate = purchaseDate;
		this.user = user;
		this.numberOfBooks = numberOfBooks;
		this.price = price;
		this.books = books;
	}
	
	public Purchase() {
		this.purchaseDate = LocalDate.now();
		this.user = new User();
		this.numberOfBooks = 0;
		this.price = 0;
	}

	public Long getID() {
		return ID;
	}

	public void setID(Long iD) {
		ID = iD;
	}

	public LocalDate getPurchaseDate() {
		return purchaseDate;
	}

	public void setPurchaseDate(LocalDate purchaseDate) {
		this.purchaseDate = purchaseDate;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public int getNumberOfBooks() {
		return numberOfBooks;
	}

	public void setNumberOfBooks(int numberOfBooks) {
		this.numberOfBooks = numberOfBooks;
	}
	
	
	public List<BoughtBook> getBooks() {
		return books;
	}

	public void setBooks(List<BoughtBook> books) {
		this.books = books;
	}

	
	
	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "Purchase [ID=" + ID + ", purchaseDate=" + purchaseDate + ", user=" + user + ", numberOfBooks="
				+ numberOfBooks + ", price=" + price + ", books=" + books + "]";
	}

	
	
	
	
	
	
}
