package com.ftn.OWPproject.model;

public class LoyaltyCard {

	private Long ID;
	private double discount;
	private int pointNumber;
	private User user;
	private LoyaltyCardStatus status = LoyaltyCardStatus.PENDING;
	
	public LoyaltyCard(Long ID, double discount, int pointNumber, User user, LoyaltyCardStatus status) {
		super();
		this.ID = ID;
		this.discount = discount;
		this.pointNumber = pointNumber;
		this.user = user;
		this.status = status;
	}

	public LoyaltyCard(double discount, int pointNumber, User user, LoyaltyCardStatus status) {
		super();
		this.discount = discount;
		this.pointNumber = pointNumber;
		this.user = user;
		this.status = status;
	}

	public Long getID() {
		return ID;
	}

	public void setID(Long iD) {
		this.ID = iD;
	}

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}

	public int getPointNumber() {
		return pointNumber;
	}

	public void setPointNumber(int pointNumber) {
		this.pointNumber = pointNumber;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	

	public LoyaltyCardStatus getStatus() {
		return status;
	}

	public void setStatus(LoyaltyCardStatus status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "LoyaltyCard [ID=" + ID + ", discount=" + discount + ", pointNumber=" + pointNumber + ", user=" + user
				+ "]";
	}
	
	
	
}
