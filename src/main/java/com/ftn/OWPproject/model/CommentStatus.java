package com.ftn.OWPproject.model;

public enum CommentStatus {

	PENDING,
	APPROVED,
	REJECTED
}
