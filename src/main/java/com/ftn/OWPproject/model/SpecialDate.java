package com.ftn.OWPproject.model;

import java.time.LocalDate;

public class SpecialDate {

	private Long ID;
	private LocalDate date;
	private double discount;
	
	public Long getID() {
		return ID;
	}
	public void setID(Long iD) {
		ID = iD;
	}
	public LocalDate getDate() {
		return date;
	}
	public void setDate(LocalDate date) {
		this.date = date;
	}
	public double getDiscount() {
		return discount;
	}
	public void setDiscount(double discount) {
		this.discount = discount;
	}
	
	public SpecialDate(Long iD, LocalDate date, double discount) {
		super();
		ID = iD;
		this.date = date;
		this.discount = discount;
	}
	
	public SpecialDate(LocalDate date, double discount) {
		this.date = date;
		this.discount = discount;
	}
	
	@Override
	public String toString() {
		return "SpecialDate [ID=" + ID + ", date=" + date + ", discount=" + discount + "]";
	}
	
	
	
	
	
}
