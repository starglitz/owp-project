package com.ftn.OWPproject.model;

public enum LoyaltyCardStatus {
	PENDING,
	REJECTED,
	ACCEPTED
}
