package com.ftn.OWPproject.model;

public class BoughtBook {
	
	private Long ID;
	private Book bookId;
	private int numberOfBooks;
	private int price;
	private Long purchase;

	
	public BoughtBook(Book bookId, int numberOfBooks, int price, Long purchase) {
		super();
		this.bookId = bookId;
		this.numberOfBooks = numberOfBooks;
		this.price = price;
		this.purchase = purchase;
		
	}
	
	public BoughtBook() {
		this.numberOfBooks = 0;
		this.price = 0;
		this.purchase = 0L;
	}

	public BoughtBook(Long iD, Book bookId, int numberOfBooks, int price, Long purchase) {
		super();
		ID = iD;
		this.bookId = bookId;
		this.numberOfBooks = numberOfBooks;
		this.price = price;
		this.purchase = purchase;

	}

	public Long getID() {
		return ID;
	}

	public void setID(Long iD) {
		ID = iD;
	}

	public Book getBookId() {
		return bookId;
	}

	public void setBookId(Book bookId) {
		this.bookId = bookId;
	}

	public int getNumberOfBooks() {
		return numberOfBooks;
	}

	public void setNumberOfBooks(int numberOfBooks) {
		this.numberOfBooks = numberOfBooks;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}
	


	public Long getPurchase() {
		return purchase;
	}

	public void setPurchaseId(Long purchase) {
		this.purchase = purchase;
	}

	@Override
	public String toString() {
		return "BoughtBook [ID=" + ID + ", bookId=" + bookId + ", numberOfBooks=" + numberOfBooks + ", price=" + price
				+ ", purchase=" + purchase + "]";
	}
	
	
	
	
	
	
	
	
}
