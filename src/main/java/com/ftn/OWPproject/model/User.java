package com.ftn.OWPproject.model;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Past;

import org.springframework.format.annotation.DateTimeFormat;

public class User {
	
	private Long ID;
	
	@NotBlank(message = "Username is mandatory")
	private String username;
	
	@NotBlank(message = "Password is mandatory")
	private String password;
	
	@NotBlank(message = "Email is mandatory")
	@Email(message = "Entered email is in invalid format")
	private String email;
	
	@NotBlank(message = "Name is mandatory")
	private String name;
	
	@NotBlank(message = "Surname is mandatory")
	private String surname; 
	
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) 
	@Past(message = "Date of birth must be before today")
	private LocalDate dateOfBirth;
	
	
	@NotBlank(message = "Address is mandatory")
	private String address;
	
	@NotBlank(message = "Contact is mandatory")
	private String contact;
	
	
	private LocalDateTime registerDate;
	private String role;

	private boolean isBlocked = false; 
	
	public User(Long ID, String username, String password, String email, String name, String surname,
			LocalDate dateOfBirth, String address, String contact, LocalDateTime registerDate, String role) {
		super();
		this.ID = ID;
		this.username = username;
		this.password = password;
		this.email = email;
		this.name = name;
		this.surname = surname;
		this.dateOfBirth = dateOfBirth;
		this.address = address;
		this.contact = contact;
		this.registerDate = registerDate;
		this.role = role;
	}
	
	public User(Long ID, String username, String password, String email, String name, String surname,
			LocalDate dateOfBirth, String address, String contact, LocalDateTime registerDate, String role, boolean isBlocked) {
		super();
		this.ID = ID;
		this.username = username;
		this.password = password;
		this.email = email;
		this.name = name;
		this.surname = surname;
		this.dateOfBirth = dateOfBirth;
		this.address = address;
		this.contact = contact;
		this.registerDate = registerDate;
		this.role = role;
		this.isBlocked = isBlocked;
	}

	public User(String username, String password, String email, String name, String surname, LocalDate dateOfBirth,
			String address, String contact, LocalDateTime registerDate, String role) {
		super();
		this.username = username;
		this.password = password;
		this.email = email;
		this.name = name;
		this.surname = surname;
		this.dateOfBirth = dateOfBirth;
		this.address = address;
		this.contact = contact;
		this.registerDate = registerDate;
		this.role = role;
	}
	
	public User() {
		this.username = "";
		this.password = "";
		this.email = "";
		this.name = "";
		this.surname = "";
		/* this.dateOfBirth = LocalDate.now(); */
		this.address = "";
		this.contact = "";
		this.registerDate = LocalDateTime.now();
		this.role = "CUSTOMER";
	}

	public Long getID() {
		return ID;
	}

	public void setID(Long ID) {
		this.ID = ID;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public LocalDate getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(LocalDate dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public LocalDateTime getRegisterDate() {
		return registerDate;
	}

	public void setRegisterDate(LocalDateTime registerDate) {
		this.registerDate = registerDate;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
	
	

	public boolean isBlocked() {
		return isBlocked;
	}

	public void setBlocked(boolean isBlocked) {
		this.isBlocked = isBlocked;
	}

	@Override
	public String toString() {
		return "User [ID=" + ID + ", username=" + username + ", password=" + password + ", email=" + email + ", name="
				+ name + ", surname=" + surname + ", dateOfBirth=" + dateOfBirth + ", address=" + address + ", contact="
				+ contact + ", registerDate=" + registerDate + ", role=" + role + "]";
	}
	
	
	
	
	
	
	
	
	
}
