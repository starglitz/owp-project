package com.ftn.OWPproject.model;

import java.util.ArrayList;


import java.util.List;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class Book {

	private Long ID;

	@NotBlank(message = "Title is mandatory")
	private String title;
	

	@NotNull(message = "Book ISBN is mandatory")
	@Positive(message = "Book ISBN must be a positive value")
	@Min(value = 1000000000000L, message = "Book ISBN should consist of 13 digits")
    @Max(value = 9999999999999L, message = "Book ISBN should consist of 13 digits")
	private Long ISBN;
	
	@NotBlank(message = "Book publisher is mandatory")
	@Size(min = 1, max = 40)
	private String publisher; 
	
	@NotNull(message = "Publish year must be a positive value")
	@Positive(message = "Publish year must be a positive value")
	@Max(value = 2021,message = "Publish year cannot be in future")
	private int publishYear;
	
	@NotBlank(message = "Author is mandatory")
	private String author;
	
	
	private String picturePath; 
	
	@NotNull(message = "Price is mandatory")
	@Positive(message = "Price must be a positive value")
	private int price;
	
	@NotNull(message = "Number of pages is mandatory")
	@Positive(message = "Number of pages must be a positive value")
	private int numberOfPages; 
	

	private Cover cover;
	
	private Writing writing;
	
	@NotBlank(message = "Book language is mandatory")
	@Size(min = 1, max = 30)
	private String language; 
	
	@Max(5)
	private double rating;
	
	@NotBlank(message = "Book description is mandatory")
	@Size(min = 1, max = 400)
	private String description;
	private List<Genre> genres = new ArrayList<>();
	
	@NotNull(message = "Number of books is mandatory")
	@Positive(message = "Number of books must be a positive value")
	private int booksLeft;
	
	public Book() {
		this.title = "";
		this.ISBN = 0L;
		this.publisher = "";
		this.publishYear = 2020;
		this.author = "";
		this.picturePath = "";
		this.price = 0;
		this.numberOfPages = 0;
		this.cover = Cover.hardcover;
		this.writing = Writing.latin;
		this.language = "";
		this.rating = 0;
		this.genres = new ArrayList<Genre>();
		this.description = "";
	}
	
	
	public Book(String title, Long iSBN, String publisher,int publishYear, String author, String picturePath, int price,
			int numberOfPages, Cover cover, Writing writing, String language, double rating,String description ,List<Genre> genres) {

		this.title = title;
		ISBN = iSBN;
		this.publisher = publisher;
		this.publishYear = publishYear;
		this.author = author;
		this.picturePath = picturePath;
		this.price = price;
		this.numberOfPages = numberOfPages;
		this.cover = cover;
		this.writing = writing;
		this.language = language;
		this.rating = rating;
		this.genres = genres;
		this.description = description;
	}
	
	
	public Book(Long ID, String title, Long iSBN, String publisher,int publishYear, String author, String picturePath, int price,
			int numberOfPages, Cover cover, Writing writing, String language, double rating, String description, int booksLeft) {
		this.ID = ID;
		this.title = title;
		this.ISBN = iSBN;
		this.publisher = publisher;
		this.publishYear = publishYear;
		this.author = author;
		this.picturePath = picturePath;
		this.price = price;
		this.numberOfPages = numberOfPages;
		this.cover = cover;
		this.writing = writing;
		this.language = language;
		this.rating = rating;
		this.description = description;
		this.booksLeft = booksLeft;
	}
	
	public Book( String title, Long iSBN, String publisher,int publishYear, String author, String picturePath, int price,
			int numberOfPages, Cover cover, Writing writing, String language, String description) {
		this.title = title;
		this.ISBN = iSBN;
		this.publisher = publisher;
		this.publishYear = publishYear;
		this.author = author;
		this.picturePath = picturePath;
		this.price = price;
		this.numberOfPages = numberOfPages;
		this.cover = cover;
		this.writing = writing;
		this.language = language;
		this.rating = 0;
		this.description = description;
	}
	
	
	public Book(Long ID,String title, Long iSBN, String publisher,int publishYear, String author, String picturePath, int price,
			int numberOfPages, Cover cover, Writing writing, String language, double rating, String description, List<Genre> genres, int booksLeft) {
		
		this.ID = ID;
		this.title = title;
		ISBN = iSBN;
		this.publisher = publisher;
		this.publishYear = publishYear;
		this.author = author;
		this.picturePath = picturePath;
		this.price = price;
		this.numberOfPages = numberOfPages;
		this.cover = cover;
		this.writing = writing;
		this.language = language;
		this.rating = rating;
		this.genres = genres;
		this.description = description;
		this.booksLeft = booksLeft;
	}

	

	public Long getID() {
		return ID;
	}

	public void setID(Long iD) {
		ID = iD;
	}

	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public Long getISBN() {
		return ISBN;
	}


	public void setISBN(Long iSBN) {
		ISBN = iSBN;
	}


	public String getPublisher() {
		return publisher;
	}


	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}


	public int getPublishYear() {
		return publishYear;
	}

	public void setPublishYear(int publishYear) {
		this.publishYear = publishYear;
	}

	public String getAuthor() {
		return author;
	}


	public void setAuthor(String author) {
		this.author = author;
	}


	public String getPicturePath() {
		return picturePath;
	}


	public void setPicturePath(String picturePath) {
		this.picturePath = picturePath;
	}


	public int getPrice() {
		return price;
	}


	public void setPrice(int price) {
		this.price = price;
	}


	public int getNumberOfPages() {
		return numberOfPages;
	}


	public void setNumberOfPages(int numberOfPages) {
		this.numberOfPages = numberOfPages;
	}


	public Cover getCover() {
		return cover;
	}


	public void setCover(Cover cover) {
		this.cover = cover;
	}


	public Writing getWriting() {
		return writing;
	}


	public void setWriting(Writing writing) {
		this.writing = writing;
	}


	public String getLanguage() {
		return language;
	}


	public void setLanguage(String language) {
		this.language = language;
	}


	public double getRating() {
		return rating;
	}


	public void setRating(double rating) {
		this.rating = rating;
	}
	

	public List<Genre> getGenres() {
		return genres;
	}

	public void setGenres(List<Genre> genres) {
		this.genres = genres;
	}
	
	

	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}

	
	

	public int getBooksLeft() {
		return booksLeft;
	}


	public void setBooksLeft(int booksLeft) {
		this.booksLeft = booksLeft;
	}


	@Override
	public String toString() {
		String retval = "Book [ID=" + ID + ", name=" + title + ", ISBN=" + ISBN + ", publisher=" + publisher + ", author="
				+ author + ", picturePath=" + picturePath + ", price=" + price + ", numberOfPages=" + numberOfPages
				+ ", cover=" + cover + ", writing=" + writing + ", language=" + language + ", rating=" + rating + " numb of books left: " + booksLeft +"]";
		for(Genre genre : genres) {
			retval += genre;
		}
		
		return retval;
	}


	
	
	
	
	
	
}
